INSERT INTO LesFilms VALUES (0,
	'Spider-Man: Far From Home',
	TO_DATE('20190626','YYYYMMDD'),
	'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQenG1fLSbkkXENCcuage2d0Dchhf9KwmmKbI_liAZ2mN3B7DZD',
	'https://www.youtube.com/embed/FguIk-SEkWI',
	q'[Devaste par la disparition d'Iron Man, son mentor, Peter Parker a pour projet de laisser son costume de super-heros derriere lui pendant quelques semaines afin de partir en vacances en Europe avec ses camarades lyceens, mais cette escapade est rapidement compromise quand il accepte a contrecoeur d'aider Nick Fury a decouvrir le mystere de plusieurs attaques de creatures, qui ravagent le continent.]');
INSERT INTO LesFilms VALUES (1,
	'Captain Marvel',
	TO_DATE('20190306','YYYYMMDD'),
	'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsUCh0H1PdHdW5lGUPL3a-Hli9ItrE7c3_Vy8-U8BJf8am0Oly',
	'https://www.youtube.com/embed/rndLWLmwgeA',
	'Synopsis');
INSERT INTO LesFilms VALUES (2,
	'Avengers : Endgame',
	TO_DATE('20190424','YYYYMMDD'),
	'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSbIzUFFVjbCs3bNx3px0sdT38SoU5hrG3mhV5hrUsieuwz62tc',
	'https://www.youtube.com/embed/wV-Q0o2OQjQ',
	'Synopsis');
INSERT INTO LesFilms VALUES (3,
	'Black Panther',
	TO_DATE('20190129','YYYYMMDD'),
	'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSU-hBA60qD87-OGzSmlq5LgO95fzm3fNASongEZRSJab26gR4l',
	'https://www.youtube.com/embed/noTuWxGmYlw',
	'Synopsis');
INSERT INTO LesFilms VALUES (4,
	'Avengers: Infinity War',
	TO_DATE('20180423','YYYYMMDD'),
	'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcS9tbiZFwDC4FqhuR7YnP4zdIZdBDZII8T0UQP49VgWt4llXmZ3',
	'https://www.youtube.com/embed/eIWs2IUr3Vs',
	'Synopsis');

INSERT INTO LesFilmsDuMois VALUES (0);
INSERT INTO LesFilmsDuMois VALUES (1);

INSERT INTO LesDVD VALUES (101, 0, 'DISPONIBLE');
INSERT INTO LesDVD VALUES (102, 0, 'DISPONIBLE');

INSERT INTO LesRealisateurs VALUES (0, 'Watts', 'Jon', TO_DATE('19870628', 'YYYYMMDD'));
INSERT INTO LesRealisateurs VALUES (1, 'Boden', 'Anna', TO_DATE('19791020', 'YYYYMMDD'));
INSERT INTO LesRealisateurs VALUES (2, 'Fleck', 'Ryan', TO_DATE('19760920', 'YYYYMMDD'));
INSERT INTO LesRealisateurs VALUES (3, 'Russo', 'Joe', TO_DATE('19710718', 'YYYYMMDD'));
INSERT INTO LesRealisateurs VALUES (4, 'Russo', 'Anthony', TO_DATE('19700203', 'YYYYMMDD'));
INSERT INTO LesRealisateurs VALUES (5, 'Coogler', 'Ryan', TO_DATE('19860523', 'YYYYMMDD'));

INSERT INTO ARealise VALUES (0, 0);
INSERT INTO ARealise VALUES (1, 1);
INSERT INTO ARealise VALUES (1, 2);
INSERT INTO ARealise VALUES (2, 3);
INSERT INTO ARealise VALUES (2, 4);
INSERT INTO ARealise VALUES (3, 5);
INSERT INTO ARealise VALUES (4, 3);
INSERT INTO ARealise VALUES (4, 4);

INSERT INTO LesActeurs VALUES (0, 'Holland', 'Tom', TO_DATE('19960601', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (1, 'Jackson', 'Samuel L.', TO_DATE('19481221', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (2, 'Coleman', 'Zendaya', TO_DATE('19960901', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (3, 'Gyllenhaal', 'Jake', TO_DATE('19801219', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (4, 'Batalon', 'Jacob', TO_DATE('19961009', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (5, 'Smulders', 'Cobie', TO_DATE('19820403', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (6, 'Favreau', 'Jon', TO_DATE('19661019', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (7, 'Brooks', 'Jerry Angelo', TO_DATE('19651216', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (8, 'Starr', 'Martin', TO_DATE('19820730', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (9, 'Tomei', 'Marisa', TO_DATE('19651204', 'YYYYMMDD'));
INSERT INTO LesActeurs (id, nom, prenom) VALUES (10, 'Acar', 'Numan');
INSERT INTO LesActeurs VALUES (11, 'Revolori', 'Tony', TO_DATE('19960428', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (12, 'Rice', 'Angourie', TO_DATE('20010101', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (13, 'Garrn', 'Toni', TO_DATE('19920707', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (14, 'Billingsley', 'Peter', TO_DATE('19710416', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (15, 'Brie', 'Larson', TO_DATE('19891001', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (16, 'Mendelsohn', 'Ben', TO_DATE('19690403', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (17, 'Bening', 'Annette', TO_DATE('19580529', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (18, 'Law', 'Jude', TO_DATE('19721229', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (19, 'Lynch', 'Lashana', TO_DATE('19871127', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (20, 'Chan', 'Gemma', TO_DATE('19821129', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (21, 'Gregg', 'Clark', TO_DATE('19620402', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (22, 'Hounsou', 'Djimon', TO_DATE('19640424', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (23, 'Pace', 'Lee', TO_DATE('19790325', 'YYYYMMDD'));

INSERT INTO LesActeurs VALUES (24, 'Downer Jr.', 'Robert', TO_DATE('19650404', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (25, 'Evans', 'Chris', TO_DATE('16870613', 'YYYYMMDD'));
INSERT INTO LesActeurs VALUES (26, 'Duke', 'Winston', TO_DATE('19861115', 'YYYYMMDD'));

INSERT INTO AJoueDans VALUES (0, 0);
INSERT INTO AJoueDans VALUES (0, 1);
INSERT INTO AJoueDans VALUES (0, 2);
INSERT INTO AJoueDans VALUES (0, 3);
INSERT INTO AJoueDans VALUES (0, 4);
INSERT INTO AJoueDans VALUES (0, 5);
INSERT INTO AJoueDans VALUES (0, 6);
INSERT INTO AJoueDans VALUES (0, 7);
INSERT INTO AJoueDans VALUES (0, 8);
INSERT INTO AJoueDans VALUES (0, 9);
INSERT INTO AJoueDans VALUES (0, 10);
INSERT INTO AJoueDans VALUES (0, 11);
INSERT INTO AJoueDans VALUES (0, 12);
INSERT INTO AJoueDans VALUES (0, 13);
INSERT INTO AJoueDans VALUES (0, 14);

INSERT INTO AJoueDans VALUES (1, 1);
INSERT INTO AJoueDans VALUES (1, 15);
INSERT INTO AJoueDans VALUES (1, 16);
INSERT INTO AJoueDans VALUES (1, 17);
INSERT INTO AJoueDans VALUES (1, 18);
INSERT INTO AJoueDans VALUES (1, 19);
INSERT INTO AJoueDans VALUES (1, 20);
INSERT INTO AJoueDans VALUES (1, 21);
INSERT INTO AJoueDans VALUES (1, 22);
INSERT INTO AJoueDans VALUES (1, 23);

INSERT INTO AJoueDans VALUES (2, 1);
INSERT INTO AJoueDans VALUES (2, 5);
INSERT INTO AJoueDans VALUES (2, 6);
INSERT INTO AJoueDans VALUES (2, 24);
INSERT INTO AJoueDans VALUES (2, 25);
INSERT INTO AJoueDans VALUES (2, 26);

INSERT INTO LesComptes (email, nom, prenom, dateNaissance, motDePasse, adresse, numeroDeTelephone)
	VALUES ('simul@tion.com', 'Simul', 'Ation', TO_DATE('19000101', 'YYYYMMDD'), 'noit@lumis', 'Simsulation', '0123456789');
INSERT INTO LesComptes (email, nom, prenom, dateNaissance, motDePasse, adresse, numeroDeTelephone)
	VALUES ('mathisperrier', 'Perrier', 'Mathis', TO_DATE('19980505', 'YYYYMMDD'), 'oui', 'adresse', '0123456789');

INSERT INTO LesAbonnements VALUES ('simul@tion.com', 'SIM-000000', '10000', TO_DATE('99991212', 'YYYYMMDD'));
