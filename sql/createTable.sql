create table LesActeurs (
	id NUMBER not null,
	nom varchar2(20) not null,
	prenom varchar2(20) not null,
	dateNaissance date,
	constraint LesActeurs_PK primary key (id),
	constraint LesActeurs_UNPD unique (nom, prenom, dateNaissance)
);

create table LesAuteurs (
	id NUMBER not null,
	nom varchar2(20) not null,
	prenom varchar2(20) not null,
	dateNaissance date,
	constraint LesAuteurs_PK primary key (id),
	constraint LesAuteurs_UNPD unique (nom, prenom, dateNaissance)
);

create table LesRealisateurs (
	id NUMBER not null,
	nom varchar2(20) not null,
	prenom varchar2(20) not null,
	dateNaissance date,
	constraint LesRealisateurs_PK primary key (id),
	constraint LesRealisateurs_UNPD unique (nom, prenom, dateNaissance)
);

create table LesFilms (
	id NUMBER not null,
	titre varchar2(100) not null,
	dateDeParution date not null,
	imgUrl varchar2(2048) not null,
	bandeAnnonceUrl varchar2(2048) not null,
	synopsis varchar2(4000),
	constraint LesFilms_PK primary key (id)
);

create table LesFilmsDuMois(
	id NUMBER not null,
	constraint LesFilmsDuMois_PK primary key (id),
	constraint LesFilmesDuMois_C1 foreign key (id) references LesFilms (id)
);

create table LesDVD (
	idDVD NUMBER not null,
	idFilm NUMBER not null,
	etat varchar2(10) not null,
	constraint LesDVD_PK primary key (idDVD),
	constraint LesDVD_C1 foreign key (idFilm) references LesFilms (id),
	constraint LesDVD_C2 check (etat IN ('DISPONIBLE', 'EMPRUNTE', 'PERDU', 'ENDOMMAGE'))
);

--Rajouter une condition de check sur l'idParent
create table LesComptes (
	email varchar2(100) not null,
	emailParent varchar2(100),
	nom varchar2(20) not null,
	prenom varchar2(20),
	dateNaissance date not null,
	motDePasse varchar2(20) not null,
	adresse varchar2(200) not null,
	numeroDeTelephone varchar2(10) not null,
	constraint LesComptes_PK primary key (email)
);

create table LesAbonnements (
	email varchar2(100) not null,
	numCarte varchar2(10) not null,
	solde NUMBER default 0,
	dateFinAbonnement date not null,
	constraint LesAbonnements_PK primary key (email, numCarte),
	constraint LesAbonnements_C1 foreign key (email) references LesComptes (email),
	constraint LesAbonnements_C2 check (solde >= 0),
	constraint LesAbonnements_UNC unique (numCarte)
);

create table AEcrit (
	idFilm NUMBER not null,
	idAuteur NUMBER not null,
	constraint AEcrit_PK primary key (idFilm, idAuteur),
	constraint AEcrit_C1 foreign key (idFilm) references LesFilms (id),
	constraint AEcrit_C2 foreign key (idAuteur) references LesAuteurs (id)
);

create table AJoueDans (
	idFilm NUMBER not null,
	idActeur NUMBER not null,
	constraint AJoueDans_PK primary key (idFilm, idActeur),
	constraint AJoueDans_C1 foreign key (idFilm) references LesFilms (id),
	constraint AJoueDans_C2 foreign key (idActeur) references LesActeurs (id)
);

create table ARealise (
	idFilm NUMBER not null,
	idRealisateur NUMBER not null,
	constraint ARealise_PK primary key (idFilm, idRealisateur),
	constraint ARealise_C1 foreign key (idFilm) references LesFilms (id),
	constraint ARealise_C2 foreign key (idRealisateur) references LesRealisateurs (id)
);

--On peut ajouter une transaction avant qu'elle soit finie
create table LesTransactions (
	id NUMBER not null,
	email varchar2(100) not null,
	idDVD NUMBER not null,
	dateDeb date not null,
	dateFin date, --dateFin > dateDeb
	constraint LesTransactions_PK primary key (id),
	constraint LesTransactions_C1 foreign key (email) references LesComptes (email),
	constraint LesTransactions_C2 foreign key (idDVD) references LesDVD (idDVD)
);

create table LesNotes (
	idFilm NUMBER not null,
	email varchar2(100) not null,
	note NUMBER not null,
	constraint LesNotes_PK primary key (idFilm, email),
	constraint LesNotes_C1 foreign key (idFilm) references LesFilms (id),
	constraint LesNotes_C2 foreign key (email) references LesComptes (email)
);
