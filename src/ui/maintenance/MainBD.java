package ui.maintenance;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import ui.layout.StackLayout;

public class MainBD extends JFrame {
	
	private static final String TITRE = "AL-2000 - Maintenance B.D.";
	
	
	
	private MainBD(String title, String[] args) {
		super(title);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		JPanel panel = new JPanel();
		StackLayout stackLayout = new StackLayout();
		JLabel labelTitre = new JLabel();
		JPanel panelTable = new JPanel();
		BorderLayout borderLayoutTable = new BorderLayout();
		JLabel labelTable = new JLabel();
		JTextField textFieldTable = new JTextField();
		JPanel panelValeurs = new JPanel();
		BorderLayout borderLayoutValeurs = new BorderLayout();
		JLabel labelValeurs = new JLabel();
		JTextField textFieldValeurs = new JTextField();
		JPanel panelInformation = new JPanel();
		JLabel labelInformation = new JLabel();
		JPanel panelButtons = new JPanel();
		JButton buttonConfirmation = new JButton();
		
		/**
		 * Bouton de confirmation
		 */
		buttonConfirmation.setText("Ajouter");
		buttonConfirmation.setFont(new Font(buttonConfirmation.getFont().getName(), Font.PLAIN, 24));
		buttonConfirmation.addActionListener(l -> {
			// TODO
			textFieldTable.setText("");
			textFieldValeurs.setText("");
			// TODO - mettre a jour le text de labelInformation
		});
		
		panelButtons.add(buttonConfirmation);
		
		/**
		 * Information de retour sur la derni�re action
		 */
		labelInformation.setText(" ");
		labelInformation.setHorizontalAlignment(JLabel.CENTER);
		
		panelInformation.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		panelInformation.add(labelInformation);
		
		/**
		 * Valeurs
		 */
		textFieldValeurs.setFont(new Font(textFieldValeurs.getFont().getName(), Font.PLAIN, 24));
		
		labelValeurs.setText("Valeurs :");
		labelValeurs.setFont(new Font(labelValeurs.getFont().getName(), Font.BOLD, 24));
		
		panelValeurs.setLayout(borderLayoutValeurs);
		panelValeurs.add(labelValeurs, BorderLayout.NORTH);
		panelValeurs.add(textFieldValeurs, BorderLayout.CENTER);
		panelValeurs.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
		
		/**
		 * Table
		 */
		textFieldTable.setFont(new Font(textFieldTable.getFont().getName(), Font.PLAIN, 24));
		
		labelTable.setText("Table :");
		labelTable.setFont(new Font(labelTable.getFont().getName(), Font.BOLD, 24));
		
		panelTable.setLayout(borderLayoutTable);
		panelTable.add(labelTable, BorderLayout.NORTH);
		panelTable.add(textFieldTable, BorderLayout.CENTER);
		panelTable.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
		
		/**
		 * Titre
		 */
		labelTitre.setText(TITRE);
		labelTitre.setFont(new Font(labelTitre.getFont().getName(), Font.BOLD, 42));
		labelTitre.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		/**
		 * Panel principal
		 */
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setLayout(stackLayout);
		panel.add(labelTitre);
		panel.add(panelTable);
		panel.add(panelValeurs);
		panel.add(panelInformation);
		panel.add(panelButtons);
		
		getContentPane().add(panel);
		
		pack();
	}
	
	public static void main(String[] argv) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { 
				new MainBD(TITRE, argv).setVisible(true); 
			}
		});
	}

}
