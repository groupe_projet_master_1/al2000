package ui.ecrans;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import ui.widgets.PanelNavigation;

public class EcranAbonnement extends JPanel {
	
	private static EcranAbonnement instance;
	
	
	
	private EcranAbonnement() {
		BorderLayout borderLayout = new BorderLayout();
		PanelNavigation panelNavigation = new PanelNavigation();
		
		panelNavigation.setButtonActionVisible(false);
		
		setLayout(borderLayout);
		add(panelNavigation, BorderLayout.NORTH);
	}
	
	public static EcranAbonnement getInstance() {
		if (instance == null) {
			instance = new EcranAbonnement();
		}
		return instance;
	}
	
}
