package ui.ecrans;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fc.films.CommandeFilm;
import ui.layout.StackLayout;
import ui.widgets.ModePayement;
import ui.widgets.PanelNavigation;
import ui.widgets.Vignette;

public class EcranLocation extends JPanel {
	
	public EcranLocation(CommandeFilm film) {
		BorderLayout borderLayout = new BorderLayout();
		PanelNavigation panelNavigation = new PanelNavigation();
		JPanel panelLocation = new JPanel();
		BorderLayout borderLayoutLocation = new BorderLayout();
		JPanel panelProduit = new JPanel();
		StackLayout stackLayoutProduit = new StackLayout();
		Vignette vignette = new Vignette(film.getVignetteURL());
		JLabel labelTitre = new JLabel(film.getTitre());
		ModePayement modePayement = new ModePayement(panelNavigation);
		
		modePayement.setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));
		
		labelTitre.setFont(new Font(labelTitre.getFont().getName(), Font.BOLD, 36));
		labelTitre.setHorizontalAlignment(JLabel.CENTER);
		
		vignette.setHorizontalAlignment(JLabel.CENTER);
		
		panelProduit.setLayout(stackLayoutProduit);
		panelProduit.add(vignette);
		panelProduit.add(labelTitre);
		
		panelLocation.setLayout(borderLayoutLocation);
		panelLocation.add(panelProduit, BorderLayout.NORTH);
		panelLocation.add(modePayement, BorderLayout.CENTER);
		
		panelNavigation.setButtonActionVisible(false);
		
		setLayout(borderLayout);
		add(panelNavigation, BorderLayout.NORTH);
		add(panelLocation, BorderLayout.CENTER);
	}
	
}
