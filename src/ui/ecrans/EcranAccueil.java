package ui.ecrans;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import ui.Main;
import ui.layout.StackLayout;
import ui.widgets.TextFieldRecherche;
import ui.widgets.Vignette;
import ui.widgets.VignetteAdapter;

public class EcranAccueil extends JScrollPane {
	
	private static EcranAccueil instance;
	
	private static final String LABEL_TITRE = "AL-2000";
	private static final String LABEL_FILMS = "- ~ - ~ - ~ - ~ - ~ - ~ -    Films du mois    - ~ - ~ - ~ - ~ - ~ - ~ -";
	private static final String GHOST_TEXT = "Film, acteur, ...";
	public static final Dimension TAILLE_VIGNETTE = new Dimension(220, 300);
	
	private JPanel panelFilms;
	private GridLayout gridLayout;
	
	
	
	public EcranAccueil() {
		JPanel panelContent = new JPanel();
		StackLayout stackLayout = new StackLayout();
		JLabel labelTitre = new JLabel();
		JPanel panelRecherche = new JPanel();
		TextFieldRecherche textFieldRecherche = new TextFieldRecherche();
		JButton buttonRecherche = new JButton();
		JLabel labelFilms = new JLabel();
		panelFilms = new JPanel();
		gridLayout = new GridLayout();
		
		gridLayout.setHgap(10);
		gridLayout.setVgap(10);
		
		panelFilms.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelFilms.setLayout(gridLayout);
		
		labelFilms.setText(LABEL_FILMS);
		labelFilms.setFont(new Font(labelFilms.getFont().getName(), Font.ITALIC, 42));
		labelFilms.setHorizontalAlignment(JLabel.CENTER);
		labelFilms.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		buttonRecherche.setText("Rechercher");
		buttonRecherche.setBackground(Color.ORANGE);
		buttonRecherche.setFont(new Font(buttonRecherche.getFont().getName(), Font.PLAIN, 24));
		buttonRecherche.setPreferredSize(new Dimension(200, 75));
		buttonRecherche.addActionListener(l -> {
			rechercher(textFieldRecherche.getText());
		});
		
		textFieldRecherche.setGhostText(GHOST_TEXT);
		textFieldRecherche.setFont(new Font(textFieldRecherche.getFont().getName(), Font.PLAIN, 52));
		textFieldRecherche.addActionListener(l -> {
			rechercher(textFieldRecherche.getText());
		});

		panelRecherche.add(textFieldRecherche);
		panelRecherche.add(buttonRecherche);
		
		labelTitre.setText(LABEL_TITRE);
		labelTitre.setFont(new Font(labelFilms.getFont().getName(), Font.BOLD, 64));
		labelTitre.setHorizontalAlignment(JLabel.CENTER);
		labelTitre.setBorder(BorderFactory.createEmptyBorder(25, 25, 25, 25));
		
		panelContent.setLayout(stackLayout);
		panelContent.add(labelTitre);
		panelContent.add(panelRecherche);
		panelContent.add(labelFilms);
		panelContent.add(panelFilms);
		
		setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		getVerticalScrollBar().setUnitIncrement(16);
		
		setViewportView(panelContent);
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { 
				Dimension d = ((JFrame) SwingUtilities.getWindowAncestor(EcranAccueil.this)).getSize();
				textFieldRecherche.setPreferredSize(new Dimension(d.width - (d.width / 3), buttonRecherche.getHeight()));
				gridLayout.setColumns(d.width / TAILLE_VIGNETTE.width);
				gridLayout.setRows(0);
			}
		});
	}
	
	public static EcranAccueil getInstance() {
		if (instance == null) {
			instance = new EcranAccueil();
		}
		return instance;
	}

	public void ajouterToutesVignettesURL(List<String> urls) {
		panelFilms.removeAll();
		for (int i=0; i<urls.size(); i++) {
			Vignette vignette = new Vignette(urls.get(i));
			vignette.setIndice(i);
			vignette.addAdapter(new VignetteAdapter(vignette));
			panelFilms.add(vignette);
		}
		panelFilms.repaint();
		panelFilms.revalidate();
	}
	
	/**
	 * Ajoute les vignettes � partir d'image
	 * @param vignettes - Liste de BufferedImage
	 */
	public void ajouterToutesVignettes(List<BufferedImage> vignettes) {
		panelFilms.removeAll();
		for (int i=0; i<vignettes.size(); i++) {
			Vignette vignette = new Vignette(vignettes.get(i));
			vignette.setIndice(i);
			vignette.addAdapter(new VignetteAdapter(vignette));
			panelFilms.add(vignette);
		}
		panelFilms.repaint();
		panelFilms.revalidate();
	}
	
	private void rechercher(String textRecherche) {
		if (textRecherche.equals(GHOST_TEXT)) {
			Main.machine.tri("");
		}
		else {
			Main.machine.tri(textRecherche);
		}
		ajouterToutesVignettes(Main.machine.getToutesVignettes());
	}
	
}
