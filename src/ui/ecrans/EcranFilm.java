package ui.ecrans;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fc.films.CommandeFilm;
import ui.Main;
import ui.widgets.InternetView;
import ui.widgets.PanelNavigation;

public class EcranFilm extends JPanel {
	
	private CommandeFilm film;
	
	private JEditorPane description;
	private InternetView internetView;
	
	
	
	public EcranFilm(CommandeFilm film) {
		this.film = film;
		
		BorderLayout borderLayout = new BorderLayout();
		PanelNavigation panelNavigation = new PanelNavigation();
		JScrollPane scrollPane = new JScrollPane();
		JPanel panelFilmElements = new JPanel();
		BorderLayout borderLayoutFilmElements = new BorderLayout();
		description = new JEditorPane();
		internetView = new InternetView(film.getBandeAnnonceURL());
		
		description.setEditable(false);
		description.setContentType("text/html");
		description.setText(construireDescription());
		description.setFont(new Font(description.getFont().getName(), Font.PLAIN, 18));
		
		panelFilmElements.setLayout(borderLayoutFilmElements);
		panelFilmElements.add(description, BorderLayout.WEST);
		panelFilmElements.add(internetView, BorderLayout.CENTER);
		
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		
		scrollPane.setViewportView(panelFilmElements);
		
		if (! Main.machine.dispoFilm()) {
			panelNavigation.setButtonActionEnabled(false);
			panelNavigation.setLabelDvdIndisponobleVisible(true);
		}
		
		setLayout(borderLayout);
		add(panelNavigation, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
	}
	
	
	
	private String construireDescription() {
		String d = "";
		d = d.concat("<div style=\"display:inline ; color:white\">");
		d = d.concat("<span style=\"font-size:28px\"><b>" + film.getTitre() + "</b></span>");
		d = d.concat("<br><br>");
		d = d.concat("<span style=\"font-size:18px\"><b>" + "Note utilisateurs (");
		d = d.concat(String.valueOf(film.getNbNotes()));
		d = d.concat(") : " + "</b></span>");
		d = d.concat("<span style=\"font-size:16px\">" + String.valueOf(film.getNote()) + "</span>");
		d = d.concat("<br><br>");
		d = d.concat("<span style=\"font-size:16px\">" + film.getSynopsis() + "</span>");
		d = d.concat("<br><br>");
		d = d.concat("<span style=\"font-size:18px\"><b>" + "Genres : " + "</b></span>");
		if (film.getRealisateur() != null) {
			d = d.concat("<span style=\"font-size:16px\">");
			for (int i=0; i<film.getGenres().size(); i++) {
				d = d.concat(film.getGenres().get(i).toString());
				if (i < film.getRealisateur().size() - 1) {
					d = d.concat(", ");
				}
			}
			d = d.concat("</span>");
		}
		d = d.concat("<br><br>");
		d = d.concat("<span style=\"font-size:18px\"><b>" + "Realisateur : " + "</b></span>");
		if (film.getRealisateur() != null) {
			d = d.concat("<span style=\"font-size:16px\">");
			for (int i=0; i<film.getRealisateur().size(); i++) {
				d = d.concat(film.getRealisateur().get(i).getPrenom() + " " + film.getRealisateur().get(i).getNom());
				if (i < film.getRealisateur().size() - 1) {
					d = d.concat(", ");
				}
			}
			d = d.concat("</span>");
		}
		d = d.concat("<br>");
		d = d.concat("<span style=\"font-size:18px\"><b>" + "Producteur : " + "</b></span>");
		if (film.getProducteur() != null) {
			d = d.concat("<span style=\"font-size:16px\">");
			for (int i=0; i<film.getProducteur().size(); i++) {
				d = d.concat(film.getProducteur().get(i).getPrenom() + " " + film.getProducteur().get(i).getNom());
				if (i < film.getProducteur().size() - 1) {
					d = d.concat(", ");
				}
			}
			d = d.concat("</span>");
		}
		d = d.concat("<br><br>");
		d = d.concat("<span style=\"font-size:18px\"><b>" + "Acteurs : " + "</b></span>");
		if (film.getActeurs() != null) {
			d = d.concat("<span style=\"font-size:16px\">");
			for (int i=0; i<film.getActeurs().size(); i++) {
				d = d.concat(film.getActeurs().get(i).getPrenom() + " " + film.getActeurs().get(i).getNom());
				if (i < film.getActeurs().size() - 1) {
					d = d.concat(", ");
				}
			}
			d = d.concat("</span>");
		}
		d = d.concat("</div>");
		return d;
	}
	
	
	
	public void setDescriptionTaille(int width, int height) {
		description.setPreferredSize(new Dimension(width, height));
	}
	
	
	
	public void clearWebView() {
		internetView.load("");
	}
	
}
