package ui.ecrans;

public enum EtatsEcran {
	VEILLE,
	ACCUEIL,
	FILM,
	LOCATION,
	ABONNEMENT
}
