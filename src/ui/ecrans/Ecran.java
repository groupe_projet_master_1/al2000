package ui.ecrans;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import ui.Main;
import ui.widgets.PanelOffreAbonnement;
import ui.widgets.PanelOffreAbonnementAdapter;
import ui.widgets.ToolBar;

public class Ecran extends JPanel {

	private static final String CHEMIN_ACCUEIL = "Accueil";
	private static final String CHEMIN_FILM = " \\ Film \\ ";
	private static final String CHEMIN_LOCATION = " \\ Location";
	private static final String CHEMIN_ABONNEMENT = " \\ Abonnement";
	
	private static Ecran instance;
	
	private EtatsEcran etatEcran;
	
	private BorderLayout borderLayout;
	private PanelOffreAbonnement panelOffreAbonnement;
	private ToolBar toolBar;
	
	private EcranFilm ecranFilm;
	
	private String chemin = "";
	
	
	
	public Ecran() {
		borderLayout = new BorderLayout();
		
		panelOffreAbonnement = new PanelOffreAbonnement();
		panelOffreAbonnement.addAdapter(new PanelOffreAbonnementAdapter(panelOffreAbonnement));
		
		toolBar = new ToolBar();
		toolBar.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(borderLayout);
		add(panelOffreAbonnement, BorderLayout.NORTH);
		add(toolBar, BorderLayout.WEST);
	}
	
	public static Ecran getInstance() {
		if (instance == null) {
			instance = new Ecran();
		}
		return instance;
	}
	
	
	
	public void majEcran() {
		if (borderLayout.getLayoutComponent(BorderLayout.CENTER) != null) {
			remove(borderLayout.getLayoutComponent(BorderLayout.CENTER));
		}
		if (ecranFilm != null) {
			ecranFilm.clearWebView();
		}
		switch (etatEcran) {
			case VEILLE:
				panelOffreAbonnement.setVisible(false);
				toolBar.setVisible(false);
				add(EcranVeille.getInstance(), BorderLayout.CENTER);
				break;
			case ACCUEIL:
				chemin = CHEMIN_ACCUEIL;
				panelOffreAbonnement.setVisible(true);
				toolBar.setVisible(true);
				add(EcranAccueil.getInstance(), BorderLayout.CENTER);
				break;
			case FILM:
				chemin = chemin.concat(CHEMIN_FILM + Main.machine.getFilmCourant().getTitre());
				panelOffreAbonnement.setVisible(true);
				toolBar.setVisible(false);
				ecranFilm = new EcranFilm(Main.machine.getFilmCourant());
				ecranFilm.setDescriptionTaille(getSize().width / 4, 0);
				add(ecranFilm, BorderLayout.CENTER);
				break;
			case LOCATION:
				chemin = chemin.concat(CHEMIN_LOCATION);
				panelOffreAbonnement.setVisible(true);
				toolBar.setVisible(false);
				EcranLocation ecranLocation = new EcranLocation(Main.machine.getFilmCourant());
				add(ecranLocation, BorderLayout.CENTER);
				break;
			case ABONNEMENT:
				chemin = chemin.concat(CHEMIN_ABONNEMENT);
				panelOffreAbonnement.setVisible(true);
				toolBar.setVisible(false);
				add(EcranAbonnement.getInstance(), BorderLayout.CENTER);
				break;
			default:
				break;
		}
		repaint();
		revalidate();
	}
	
	public void setEtat(EtatsEcran nouvelEtat) {
		etatEcran = nouvelEtat;
		majEcran();
	}
	
	public void setEtatPrecedent() {
		switch (etatEcran) {
			case VEILLE:
				break;
			case ACCUEIL:
				break;
			case FILM:
				etatEcran = EtatsEcran.ACCUEIL;
				break;
			case LOCATION:
				chemin = CHEMIN_ACCUEIL;
				etatEcran = EtatsEcran.FILM;
				break;
			case ABONNEMENT:
				chemin = CHEMIN_ACCUEIL;
				etatEcran = EtatsEcran.ACCUEIL;
			default:
				break;
		}
		majEcran();
	}
	
	
	
	public String getChemin() {
		return chemin;
	}
	
}
