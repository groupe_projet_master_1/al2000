package ui.ecrans;

import java.awt.Color;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import ui.widgets.InternetView;

public class EcranVeille extends JLayeredPane {
	
	private static EcranVeille instance;
	
	
	
	private EcranVeille() {
		InternetView publicite = new InternetView("");
		JPanel information = new JPanel();
		
		information.setBackground(Color.RED);
		information.setOpaque(true);
		information.setBounds(0, 0, 100, 100);
		
		add(publicite, 0);
		//add(information, 1);
		
		repaint();
		revalidate();
	}
	
	public static EcranVeille getInstance() {
		if (instance == null) {
			instance = new EcranVeille();
		}
		return instance;
	}
	
}