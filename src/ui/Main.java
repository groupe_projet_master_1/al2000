package ui;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.jtattoo.plaf.noire.NoireLookAndFeel;

import fc.CommandeMachine;
import fc.Machine;
import ui.ecrans.Ecran;
import ui.ecrans.EcranAccueil;
import ui.ecrans.EtatsEcran;

public class Main extends JFrame {
	
	public static CommandeMachine machine;
	
	private Main(String title, String[] args) {
		super(title);
		
		try {
			UIManager.setLookAndFeel(NoireLookAndFeel.class.getName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		machine = new Machine();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setUndecorated(true);
		
		Ecran.getInstance().setEtat(EtatsEcran.ACCUEIL);
		
		EcranAccueil.getInstance().ajouterToutesVignettes(machine.getToutesVignettes());
		
		add(Ecran.getInstance());
		
		pack();
	}
	
	public static void main(String[] argv) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() { 
				new Main("AL-2000", argv).setVisible(true); 
			}
		});
	}
	
}
