package ui.simulation;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FenteCarte implements KeyListener {
	
	private static final int BOUTON_ACTION = KeyEvent.VK_F1;
	
	private static enum Etats {
		CARTE_RETIREE,
		INSERTION,
		CARTE_INSEREE,
		EXTRACTION
	};
	
	private Etats etat = Etats.CARTE_RETIREE;
	
	private SimulationFenteCarte simulation;
	
	
	
	public FenteCarte(SimulationFenteCarte sim) {
		simulation = sim;
	}
	
	
	
	@Override
	public void keyTyped(KeyEvent e) {
		return;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getExtendedKeyCode() == BOUTON_ACTION) {
			switch (etat) {
				case CARTE_RETIREE:
					etat = Etats.INSERTION;
					break;
				case INSERTION:
					break;
				case CARTE_INSEREE:
					etat = Etats.EXTRACTION;
					break;
				case EXTRACTION:
					break;
				default:
					break;
			}
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getExtendedKeyCode() == BOUTON_ACTION) {
			switch (etat) {
				case CARTE_RETIREE:
					break;
				case INSERTION:
					etat = Etats.CARTE_INSEREE;
					simulation.carteInseree();
					break;
				case CARTE_INSEREE:
					break;
				case EXTRACTION:
					etat = Etats.CARTE_RETIREE;
					simulation.carteRetiree();
					break;
				default:
					break;
			}
		}
	}
	
	
	
	public String[] getDonneesCarteBancaire() {
		String numero = "1234123412341234";
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.getInstance().get(Calendar.YEAR + 1), 12, 31);
		String dateExpiration = SimpleDateFormat.getDateInstance().format(calendar.getTime());
		String cryptogramme = "123";
		return new String[] {numero, dateExpiration, cryptogramme};
	}
	

	
	public String[] getDonneesCarteAbonnee() {
		String email = "simul@tion.com";
		String mdp = "noit@lumis";
		return new String[] {email, mdp};
	}
	
}
