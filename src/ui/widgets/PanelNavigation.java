package ui.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ui.ecrans.Ecran;
import ui.ecrans.EtatsEcran;

public class PanelNavigation extends JPanel {

	private JButton buttonAccueil;
	private JButton buttonPrecedent;
	private JLabel labelDvdIndisponoble;
	private JButton buttonAction;
	
	
	
	public PanelNavigation() {
		BorderLayout borderLayout = new BorderLayout();
		JLabel chemin = new JLabel();
		JPanel navigationButtons = new JPanel();
		buttonAccueil = new JButton("Accueil");
		buttonPrecedent = new JButton("Precedent");
		JPanel panelBoutonAction = new JPanel();
		labelDvdIndisponoble = new JLabel();
		buttonAction = new JButton("Louer ce film");
		
		buttonAction.setBackground(Color.ORANGE);
		buttonAction.setFont(new Font(buttonAction.getFont().getName(), Font.PLAIN, 24));
		buttonAction.setPreferredSize(new Dimension(200, 75));
		
		labelDvdIndisponoble.setText("DVD Indisponible");
		labelDvdIndisponoble.setFont(new Font(labelDvdIndisponoble.getFont().getName(), Font.PLAIN, 24));
		labelDvdIndisponoble.setForeground(Color.ORANGE);
		labelDvdIndisponoble.setVisible(false);
		
		panelBoutonAction.add(labelDvdIndisponoble);
		panelBoutonAction.add(buttonAction);
		panelBoutonAction.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
		
		buttonPrecedent.setFont(new Font(buttonPrecedent.getFont().getName(), Font.PLAIN, 24));
		buttonPrecedent.setPreferredSize(new Dimension(200, 75));
		
		buttonAccueil.setFont(new Font(buttonAccueil.getFont().getName(), Font.PLAIN, 24));
		buttonAccueil.setPreferredSize(new Dimension(200, 75));
		
		navigationButtons.add(buttonAccueil);
		navigationButtons.add(buttonPrecedent);
		navigationButtons.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
		
		chemin.setText(Ecran.getInstance().getChemin());
		chemin.setFont(new Font(chemin.getFont().getName(), Font.ITALIC, 24));
		chemin.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
		
		setLayout(borderLayout);
		add(chemin, BorderLayout.NORTH);
		add(navigationButtons, BorderLayout.WEST);
		add(panelBoutonAction, BorderLayout.EAST);
		
		initialiseEvenements();
	}
	
	
	
	private void initialiseEvenements() {
		buttonAccueil.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Ecran.getInstance().setEtat(EtatsEcran.ACCUEIL);
			}
		});
		
		buttonPrecedent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Ecran.getInstance().setEtatPrecedent();
			}
		});
		
		buttonAction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Ecran.getInstance().setEtat(EtatsEcran.LOCATION);
			}
		});
	}
	
	
	
	public void setLabelDvdIndisponobleVisible(boolean b) {
		labelDvdIndisponoble.setVisible(b);
	}
	
	public void setButtonActionVisible(boolean b) {
		buttonAction.setVisible(b);
	}
	
	public void setButtonActionEnabled(boolean b) {
		buttonAction.setEnabled(b);
	}
	
	
	
	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(b);
		buttonAccueil.setEnabled(b);
		buttonPrecedent.setEnabled(b);
		buttonAction.setEnabled(b);
	}
	
}
