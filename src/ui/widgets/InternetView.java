package ui.widgets;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebView;

public class InternetView extends JFXPanel {
	
	private WebView webView;
	
	
	
	public InternetView(String url) {
		Platform.setImplicitExit(false);
		Platform.runLater(new Runnable() {
            @Override
            public void run() {
    			webView = new WebView();
    			webView.getEngine().load(url);
    			setScene(new Scene(webView));
            }
        });
	}
	
	
	
	public void load(String url) {
		Platform.runLater(new Runnable() {
            @Override
            public void run() {
        		webView.getEngine().load(url);
            }
        });
	}
	
}
