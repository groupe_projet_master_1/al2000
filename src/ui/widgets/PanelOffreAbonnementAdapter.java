package ui.widgets;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

import ui.ecrans.Ecran;
import ui.ecrans.EtatsEcran;

public class PanelOffreAbonnementAdapter extends MouseAdapter {
	
	private enum Etats {
		IDLE,
		PRESSING_IN,
		PRESSING_OUT
	}
	
	private Etats etat = Etats.IDLE;
	
	private JComponent component;
	
	
	
	public PanelOffreAbonnementAdapter(JComponent component) {
		this.component = component;
	}
	
	
	
	@Override
	public void mousePressed(MouseEvent e) {
		etat = Etats.PRESSING_IN;
		component.setEnabled(false);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (etat == Etats.PRESSING_IN) {
			Ecran.getInstance().setEtat(EtatsEcran.ABONNEMENT);
		}
		etat = Etats.IDLE;
		component.setEnabled(true);
	}
	
	@Override
    public void mouseEntered(MouseEvent e) {
		if (etat == Etats.PRESSING_OUT) {
			etat = Etats.PRESSING_IN;
			component.setEnabled(false);
		}
	}
	
	@Override
    public void mouseExited(MouseEvent e) {
		if (etat == Etats.PRESSING_IN) {
			etat = Etats.PRESSING_OUT;
			component.setEnabled(true);
		}
	}
	
}
