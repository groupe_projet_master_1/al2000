package ui.widgets;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import ui.Main;
import ui.ecrans.Ecran;
import ui.ecrans.EtatsEcran;

public class VignetteAdapter extends MouseAdapter {

	private static final Cursor CURSEUR_DEFAUT = new Cursor(Cursor.DEFAULT_CURSOR);
	private static final Cursor CURSEUR_MAIN = new Cursor(Cursor.HAND_CURSOR);
	
	private enum Etats {
		IDLE,
		PRESSING_IN,
		PRESSING_OUT
	}
	
	private Etats etat = Etats.IDLE;
	
	private Vignette vignette;
	
	
	
	public VignetteAdapter(Vignette vignette) {
		this.vignette = vignette;
	}
	
	
	
	@Override
	public void mousePressed(MouseEvent e) {
		etat = Etats.PRESSING_IN;
		vignette.setEnabled(false);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (etat == Etats.PRESSING_IN) {
			Main.machine.setFilmCourant(vignette.getIndice());
			Ecran.getInstance().setEtat(EtatsEcran.FILM);
		}
		etat = Etats.IDLE;
		vignette.setEnabled(true);
	}
	
	@Override
    public void mouseEntered(MouseEvent e) {
		vignette.setCursor(CURSEUR_MAIN);
		if (etat == Etats.PRESSING_OUT) {
			etat = Etats.PRESSING_IN;
			vignette.setEnabled(false);
		}
	}
	
	@Override
    public void mouseExited(MouseEvent e) {
		vignette.setCursor(CURSEUR_DEFAUT);
		if (etat == Etats.PRESSING_IN) {
			etat = Etats.PRESSING_OUT;
			vignette.setEnabled(true);
		}
	}
	
}
