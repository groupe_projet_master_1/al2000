package ui.widgets;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelOffreAbonnement extends JPanel {
	
	public PanelOffreAbonnement() {
		JLabel labelOffre = new JLabel();
		
		labelOffre.setText("<cliquez ici>   Abonnement : Offre Noel -50%   <cliquez ici>");
		labelOffre.setForeground(Color.ORANGE);
		labelOffre.setFont(new Font(labelOffre.getFont().getName(), Font.PLAIN, 28));
		labelOffre.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setBorder(BorderFactory.createLineBorder(Color.ORANGE, 3));
		add(labelOffre);
	}
	
	
	
	public void addAdapter(MouseAdapter adapter) {
		addMouseListener(adapter);
		addMouseMotionListener(adapter);
	}
	
}
