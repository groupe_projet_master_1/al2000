package ui.widgets;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import ui.ecrans.EcranAccueil;

public class Vignette extends JLabel {
	
	private static final String AFFICHE_DEFAUT = "images/affiche_defaut.png";
	
	private int indice = -1;
	
	public Vignette() {
		this(AFFICHE_DEFAUT);
	}
	
	public Vignette(String path) {
		super();
		
		setVignette(path);
	}
	
	/**
	 * Constructeur � partir d'une image
	 * @param image
	 */
	public Vignette(BufferedImage image) {
		super();
		Image imageVignette = image.getScaledInstance(EcranAccueil.TAILLE_VIGNETTE.width, EcranAccueil.TAILLE_VIGNETTE.height, Image.SCALE_SMOOTH);
		setIcon(new ImageIcon(imageVignette));
	}
	
	public void addAdapter(MouseAdapter adapter) {
		addMouseListener(adapter);
		addMouseMotionListener(adapter);
	}
	
	public void setIndice(int i) {
		indice = i;
	}
	
	private void setVignette(String path) {
		try {
			BufferedImage image = ImageIO.read(new URL(path));
			Image imageVignette = image.getScaledInstance(EcranAccueil.TAILLE_VIGNETTE.width, EcranAccueil.TAILLE_VIGNETTE.height, Image.SCALE_SMOOTH);
			setIcon(new ImageIcon(imageVignette));
		}
		catch (IOException e) {
			try {
				BufferedImage image = ImageIO.read(new File(AFFICHE_DEFAUT));
				setIcon(new ImageIcon(image));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public int getIndice() {
		return indice;
	}
	
}
