package ui.widgets;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

import ui.layout.StackLayout;

public class ToolBar extends JPanel {
	
	private static final Dimension TAILLE_BOUTTON = new Dimension(100, 100);
	
	
	
	public ToolBar() {
		BorderLayout borderLayout = new BorderLayout();
		JPanel panelUtilisateur = new JPanel();
		StackLayout stackLayoutUtilisateur = new StackLayout();
		JButton buttonCompte = new JButton();
		JButton buttonRendreDVD = new JButton();
		JPanel panelMaintenance = new JPanel();
		StackLayout stackLayoutMaintenance = new StackLayout();
		JButton buttonSOS = new JButton();
		
		buttonSOS.setText("S.O.S");
		buttonSOS.setFont(new Font(buttonSOS.getFont().getName(), Font.PLAIN, 24));
		buttonSOS.setPreferredSize(TAILLE_BOUTTON);
		
		panelMaintenance.setLayout(stackLayoutMaintenance);
		panelMaintenance.add(buttonSOS);
		
		buttonRendreDVD.setText("<html><center>Rendre<br />DVD</center></html>");
		buttonRendreDVD.setFont(new Font(buttonRendreDVD.getFont().getName(), Font.PLAIN, 24));
		buttonRendreDVD.setPreferredSize(TAILLE_BOUTTON);
		
		buttonCompte.setText("<html><center>Mon<br />Compte</center></html>");
		buttonCompte.setFont(new Font(buttonCompte.getFont().getName(), Font.PLAIN, 24));
		buttonCompte.setPreferredSize(TAILLE_BOUTTON);
		
		panelUtilisateur.setLayout(stackLayoutUtilisateur);
		panelUtilisateur.add(buttonCompte);
		panelUtilisateur.add(buttonRendreDVD);
		
		setLayout(borderLayout);
		add(panelUtilisateur, BorderLayout.CENTER);
		add(panelMaintenance, BorderLayout.SOUTH);
	}
	
}
