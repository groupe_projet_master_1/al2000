package ui.widgets;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

public class TextFieldRecherche extends JTextField {
	
	private static String DEFAULT_GHOST_TEXT = "";
	
	private static Color DEFAULT_TEXT_COLOR;
	private static final Color DEFAULT_GHOST_TEXT_COLOR = new Color(0, 0, 0, 100);
	
	
	
	public TextFieldRecherche () {
		DEFAULT_TEXT_COLOR = getSelectedTextColor();
		
		addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if (getText().trim().equals("")) {
					setForeground(DEFAULT_GHOST_TEXT_COLOR);
					setText(DEFAULT_GHOST_TEXT);
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				if (getText().trim().equals(DEFAULT_GHOST_TEXT)) {
					setForeground(DEFAULT_TEXT_COLOR);
					setText("");
				}
			}
			
		});
	}
	
	
	
	public void setGhostText(String text) {
		DEFAULT_GHOST_TEXT = text;
	}
	
}
