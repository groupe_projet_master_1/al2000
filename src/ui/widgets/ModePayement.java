package ui.widgets;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import fc.ModePaiement;
import ui.Main;
import ui.layout.StackLayout;

public class ModePayement extends JPanel {
	
	private JComponent componentADesactiver;
	
	
	
	public ModePayement() {
		this(null);
	}
	
	public ModePayement(JComponent componentADesactiver) {
		this.componentADesactiver = componentADesactiver;
		
		BorderLayout borderLayout = new BorderLayout();
		JSplitPane splitPane = new JSplitPane();
		JLabel labelModePayement = new JLabel("Choisissez votre mode de paiement :");
		// Avec abonnement
		JPanel panelAvecAbonnement = new JPanel();
		StackLayout stackLayoutAvecAbonnement = new StackLayout();
		JLabel labelAvecAbonnement = new JLabel("Avec abonnement : ");
		JLabel labelPrixAvecAbonnement = new JLabel("$" + Main.machine.getPrixAdherent());
		JPanel panelBoutonAvecAbonnement = new JPanel();
		JButton buttonPayerAvecAbonnement = new JButton("Carte abonnement");
		// Sans abonnement
		JPanel panelSansAbonnement = new JPanel();
		StackLayout stackLayoutSansAbonnement = new StackLayout();
		JLabel labelSansAbonnement = new JLabel("Sans Abonnement : ");
		JLabel labelPrixSansAbonnement = new JLabel("$" + Main.machine.getPrixNonAdherent());
		JPanel panelBoutonSansAbonnement = new JPanel();
		JButton buttonPayerSansAbonnement = new JButton("Carte bancaire");
		
		/**
		 * Sans abonnement
		 */
		buttonPayerSansAbonnement.setFont(new Font(buttonPayerSansAbonnement.getFont().getName(), Font.PLAIN, 52));
		buttonPayerSansAbonnement.setPreferredSize(new Dimension(500, 150));
		buttonPayerSansAbonnement.addActionListener(l -> {
			afficherPayementCarte(ModePaiement.CB);
		});
		
		panelBoutonSansAbonnement.add(buttonPayerSansAbonnement);
		
		labelPrixSansAbonnement.setFont(new Font(labelPrixSansAbonnement.getFont().getName(), Font.BOLD, 92));
		labelPrixSansAbonnement.setHorizontalAlignment(JLabel.CENTER);
		
		labelSansAbonnement.setFont(new Font(labelSansAbonnement.getFont().getName(), Font.BOLD, 38));
		labelSansAbonnement.setHorizontalAlignment(JLabel.CENTER);
		
		panelSansAbonnement.setLayout(stackLayoutSansAbonnement);
		panelSansAbonnement.add(labelSansAbonnement);
		panelSansAbonnement.add(labelPrixSansAbonnement);
		panelSansAbonnement.add(panelBoutonSansAbonnement);
		
		/**
		 * Avec abonnement
		 */
		buttonPayerAvecAbonnement.setFont(new Font(buttonPayerAvecAbonnement.getFont().getName(), Font.PLAIN, 52));
		buttonPayerAvecAbonnement.setPreferredSize(new Dimension(500, 150));
		buttonPayerAvecAbonnement.addActionListener(l -> {
			afficherPayementCarte(ModePaiement.SOLDE);
		});
		
		panelBoutonAvecAbonnement.add(buttonPayerAvecAbonnement);
		
		labelPrixAvecAbonnement.setFont(new Font(labelPrixAvecAbonnement.getFont().getName(), Font.BOLD, 92));
		labelPrixAvecAbonnement.setHorizontalAlignment(JLabel.CENTER);
		
		labelAvecAbonnement.setFont(new Font(labelAvecAbonnement.getFont().getName(), Font.BOLD, 38));
		labelAvecAbonnement.setHorizontalAlignment(JLabel.CENTER);
		
		panelAvecAbonnement.setLayout(stackLayoutAvecAbonnement);
		panelAvecAbonnement.add(labelAvecAbonnement);
		panelAvecAbonnement.add(labelPrixAvecAbonnement);
		panelAvecAbonnement.add(panelBoutonAvecAbonnement);
		
		
		
		labelModePayement.setFont(new Font(labelModePayement.getFont().getName(), Font.PLAIN, 52));
		labelModePayement.setHorizontalAlignment(JLabel.CENTER);
		
		splitPane.setResizeWeight(0.5);
		splitPane.setDividerSize(2);
		splitPane.add(panelAvecAbonnement, JSplitPane.LEFT);
		splitPane.add(panelSansAbonnement, JSplitPane.RIGHT);
		
		setLayout(borderLayout);
		add(labelModePayement, BorderLayout.NORTH);
		add(splitPane, BorderLayout.CENTER);
	}
	
	
	
	private void afficherPayementCarte(ModePaiement modePaiement) {
		Main.machine.setModePaiement(modePaiement);
		Container parent = getParent();
		parent.add(new PayementCarte(componentADesactiver));
		parent.remove(this);
		parent.repaint();
		parent.revalidate();
	}
	
}
