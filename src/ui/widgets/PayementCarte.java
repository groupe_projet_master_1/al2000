package ui.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fc.RetourLouer;
import ui.Main;
import ui.ecrans.Ecran;
import ui.ecrans.EtatsEcran;
import ui.layout.StackLayout;
import ui.simulation.FenteCarte;
import ui.simulation.SimulationFenteCarte;

public class PayementCarte extends JPanel implements SimulationFenteCarte {
	
	private static final String CARTE_CREDIT = "carte de credit";
	private static final String CARTE_ABONNEMENT = "carte d'abonnement";
	
	public static enum Etats {
		PAIEMENT_VALIDATION,
		ATTENTE_INSERTION_CARTE_CREDIT,
		ATTENTE_INSERTION_CARTE_ABONNEMENT,
		TRANSACTION_EN_COURS,
		ATTENTE_RETIRER_CARTE
	};
	
	private Etats etat;
	
	private JComponent componentADesactiver;
	
	private FenteCarte fenteCarte;
	
	private String carteNom;
	
	private BorderLayout borderLayout;
	
	private JPanel panelValidation;
	private JPanel panelInsererCarte;
	private JPanel panelTransactionEnCours;
	private JPanel panelFinal;
	private JLabel labelEtatPayement;
	private JLabel labelNotification;
	
	
	
	public PayementCarte(JComponent componentADesactiver) {
		this.componentADesactiver = componentADesactiver;
		
		switch (Main.machine.getModePaiement()) {
			case CB:
				carteNom = CARTE_CREDIT;
				break;
			case SOLDE:
				carteNom = CARTE_ABONNEMENT;
				break;
			default:
				break;
		}
		
		etat = Etats.PAIEMENT_VALIDATION;
		
		fenteCarte = new FenteCarte(this);
		
		borderLayout = new BorderLayout();

		initialisationPanelValidation();
		initialisationPanelInsererCarte();
		initialisationPanelTransactionEnCours();
		initialisationPanelFinal();
		
		setLayout(borderLayout);
		setFocusable(true);
		
		majAffichage();
	}
	

	
	private void initialisationPanelValidation() {
		panelValidation = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		JPanel panelContenu = new JPanel();
		StackLayout stackLayout = new StackLayout();
		JLabel labelNotification = new JLabel();
		JPanel panelBouttons = new JPanel();
		JPanel panelConfirmer = new JPanel();
		JButton buttonConfirmer = new JButton("Payer");
		JPanel panelAnnuler = new JPanel();
		JButton buttonAnnuler = new JButton("Annuler");
		
		buttonAnnuler.setFont(new Font(labelNotification.getFont().getName(), Font.PLAIN, 38));
		buttonAnnuler.setPreferredSize(new Dimension(300, 150));
		buttonAnnuler.addActionListener(l -> {
			Ecran.getInstance().setEtat(EtatsEcran.LOCATION);
		});
		
		panelAnnuler.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelAnnuler.add(buttonAnnuler);
		
		buttonConfirmer.setFont(new Font(labelNotification.getFont().getName(), Font.PLAIN, 38));
		buttonConfirmer.setPreferredSize(new Dimension(300, 150));
		buttonConfirmer.addActionListener(l -> {
			switch (Main.machine.getModePaiement()) {
				case CB:
					setEtat(Etats.ATTENTE_INSERTION_CARTE_CREDIT);
					break;
				case SOLDE:
					setEtat(Etats.ATTENTE_INSERTION_CARTE_ABONNEMENT);
					break;
				default:
					break;
			}
		});
		
		panelConfirmer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panelConfirmer.add(buttonConfirmer);
		
		panelBouttons.add(panelConfirmer);
		panelBouttons.add(panelAnnuler);
		
		labelNotification.setText("Souhaitez-vous payer ?");
		labelNotification.setFont(new Font(labelNotification.getFont().getName(), Font.PLAIN, 38));
		labelNotification.setHorizontalAlignment(JLabel.CENTER);
		
		panelContenu.setLayout(stackLayout);
		panelContenu.add(labelNotification);
		panelContenu.add(panelBouttons);
		
		panelValidation.setLayout(gridBagLayout);
		panelValidation.add(panelContenu, constraints);
	}
	
	private void initialisationPanelInsererCarte() {
		panelInsererCarte = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		JLabel labelNotification = new JLabel();
		
		labelNotification.setFont(new Font(labelNotification.getFont().getName(), Font.PLAIN, 38));
		labelNotification.setText("Inserez votre " + carteNom + " dans la fente dediee...");
		
		panelInsererCarte.setLayout(gridBagLayout);
		panelInsererCarte.add(labelNotification, constraints);
	}
	
	private void initialisationPanelTransactionEnCours() {
		panelTransactionEnCours = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		JLabel labelNotification = new JLabel();
		
		labelNotification.setFont(new Font(labelNotification.getFont().getName(), Font.PLAIN, 38));
		labelNotification.setText("<html><center>Transaction en cours... Veuillez patienter.</center></html>");
		
		panelTransactionEnCours.setLayout(gridBagLayout);
		panelTransactionEnCours.add(labelNotification, constraints);
	}
	
	private void initialisationPanelFinal() {
		panelFinal = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		JPanel panelContenu = new JPanel();
		StackLayout stackLayout = new StackLayout();
		labelEtatPayement = new JLabel();
		labelNotification = new JLabel();
		
		labelNotification.setFont(new Font(labelNotification.getFont().getName(), Font.PLAIN, 38));
		
		labelEtatPayement.setFont(new Font(labelEtatPayement.getFont().getName(), Font.PLAIN, 38));
		labelEtatPayement.setForeground(Color.ORANGE);
		labelEtatPayement.setHorizontalAlignment(JLabel.CENTER);
		labelEtatPayement.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		panelContenu.setLayout(stackLayout);
		panelContenu.add(labelEtatPayement);
		panelContenu.add(labelNotification);
		
		panelFinal.setLayout(gridBagLayout);
		panelFinal.add(panelContenu, constraints);
	}
	
	
	
	private void majAffichage() {
		if (borderLayout.getLayoutComponent(BorderLayout.CENTER) != null) {
			remove(borderLayout.getLayoutComponent(BorderLayout.CENTER));
		}
		switch (etat) {
			case PAIEMENT_VALIDATION:
				desactiverComponent();
				add(panelValidation, BorderLayout.CENTER);
				break;
			case ATTENTE_INSERTION_CARTE_CREDIT:
				desactiverComponent();
				add(panelInsererCarte, BorderLayout.CENTER);
				addKeyListener(fenteCarte);
				break;
			case ATTENTE_INSERTION_CARTE_ABONNEMENT:
				desactiverComponent();
				add(panelInsererCarte, BorderLayout.CENTER);
				addKeyListener(fenteCarte);
				break;
			case TRANSACTION_EN_COURS:
				desactiverComponent();
				add(panelTransactionEnCours, BorderLayout.CENTER);
				break;
			case ATTENTE_RETIRER_CARTE:
				desactiverComponent();
				add(panelFinal, BorderLayout.CENTER);
				addKeyListener(fenteCarte);
				break;
			default:
				break;
		}
		repaint();
		revalidate();
		requestFocus();
	}
	
	public void setEtat(Etats nouvelEtat) {
		etat = nouvelEtat;
		majAffichage();
	}
	

	
	private void desactiverComponent() {
		if (componentADesactiver != null) {
			componentADesactiver.setEnabled(false);
		}
	}
	
	
	
	private void louerDVD() {
		RetourLouer resultatLocation = Main.machine.louerDVD();
		switch (resultatLocation) {
			case LOUE:
				labelEtatPayement.setText("Paiement effectue.");
				labelNotification.setText("<html><center>Veuillez retirer votre " + carteNom + " et n'oubliez pas votre DVD.<br>Bon visionage !</center></html>");
				break;
			case DVD_INDISPO:
				labelEtatPayement.setText("Paiement echoue : DVD indisponible.");
				labelNotification.setText("Veuillez retirer votre " + carteNom + ".");
				break;
			case FILM_NON_SELECTIONNE:
				labelEtatPayement.setText("ERREUR : Aucun film selectione");
				labelNotification.setText("Veuillez retirer votre " + carteNom + ".");
				break;
			case LIMITE_NB_LOCATIONS_DEPASSE:
				labelEtatPayement.setText("Paiement echoue : Votre limite de location a ete atteint.");
				labelNotification.setText("Veuillez retirer votre " + carteNom + ".");
				break;
			case NON_CONNECTE:
				labelEtatPayement.setText("ERREUR : utilisateur non-connecte");
				labelNotification.setText("Veuillez retirer votre " + carteNom + ".");
				break;
			case PAIEMENT_ECHOUE:
				labelEtatPayement.setText("Paiement echoue : Verifiez votre solde ou contactez votre banque.");
				labelNotification.setText("Veuillez retirer votre " + carteNom + ".");
				break;
			case SOLDE_INSUFFISANT:
				labelEtatPayement.setText("Paiement echoue : Solde sur la carte d'abonnee insuffisante.");
				labelNotification.setText("Veuillez retirer votre " + carteNom + ".");
				break;
			default:
				break;
		}
		setEtat(Etats.ATTENTE_RETIRER_CARTE);
	}
	
	
	
	@Override
	public void carteInseree() {
		removeKeyListener(fenteCarte);
		setEtat(Etats.TRANSACTION_EN_COURS);
		switch (Main.machine.getModePaiement()) {
			case CB:
				Main.machine.setDonneesCB(fenteCarte.getDonneesCarteBancaire());
				break;
			case SOLDE:
				Main.machine.authentification(fenteCarte.getDonneesCarteAbonnee()[0], fenteCarte.getDonneesCarteAbonnee()[1]);
				break;
			default:
				break;
		}
		louerDVD();
	}
	
	@Override
	public void carteRetiree() {
		removeKeyListener(fenteCarte);
		Ecran.getInstance().setEtat(EtatsEcran.ACCUEIL);
	}
	
}
