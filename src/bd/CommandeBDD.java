package bd;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import fc.compte.Abonnement;
import fc.compte.Utilisateur;
import fc.films.DVD;
import fc.films.Film;
import fc.films.Etats;


public interface CommandeBDD {

	/**
	 * Permet d'ajouter un abonnement  un utilisateur
	 * @param abonnement
	 * @param user
	 * @return vrai si l'ajout s'est effectue
	 */
	public boolean ajoutAbonnementUtilisateur(Abonnement abonnement, Utilisateur user);

	/**
	 * Permet de savoir si l'email est deja utilise ou non
	 * @param string
	 * @return vrai si l'email est deja utilise, faux sinon
	 */
	public boolean emailDejaUtilise(String string);

	/**
	 * Enregistre un nouvel utilisateur dans la BA
	 * @param nouveau
	 * @return vrai si l'insertion s'est bien passe
	 */
	public boolean enregistrerUtilisateur(Utilisateur nouveau);
	
	/**
	 * Permet de jouer un dvd
	 * @param dvdALouer
	 * @return vrai si la modification du DVD s'est bien passe
	 */
	public boolean louerFilm(DVD dvdALouer);
	
	/**
	 * Permet de modifier le solde de l'utilisateur u 
	 * @param u
	 * @param montant - ajout ou suppresion du montant (si positif = ajout, si negatif = retrait)
	 * @return vrai si la modification s'est faite
	 */
	public boolean modifierSolde(Utilisateur u, float montant);

	/**
	 * Permet de retourner le dvd d'id dvdId a l'etat e
	 * Fonction a utiliser pour la perte ou la casse
	 * @param dvdId
	 * @param e
	 * @return vrai si tout s'est bien passe, faux sinon
	 */
	public boolean retournerDvd(long dvdId, Etats e);

	/**
	 * Permet de retourner le dvd d'id dvdId a l'etat e avec la note note
	 * @param dvdId
	 * @param email 
	 * @param e
	 * @param note
	 * @return vrai si tout s'est bien passe, faux sinon
	 */
	public boolean retournerDvd(long dvdId, String email, Etats e, double note);

	/**
	 * Permet de noter le filme d'id idFilm avec la note note
	 * @param idFilm
	 * @param email 
	 * @param note
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean noterFilm(long idFilm, String email, double note);
	
	/**
	 * Retourne un DVD du film f
	 * @param f
	 * @return DVD si un dvd est disponible, null sinon
	 */
	public DVD dispo(Film f);

	/**
	 * Retourne une liste de film en fonction des filtres
	 * Fonction non sensible a la casse
	 * @param germe Liste des filtres
	 * @return
	 */
	public List<Film> filtre(List<String> germe);
	
	/**
	 * Permet de connaitre tous les films du mois
	 * @return
	 */
	public List<Film> filmDisponibles();
	
	/**
	 * Permet de connaitre l'email, l'id du dvd et le titre du film des transactions qui se sont passee il y a nombreDeJours jours
	 * @param nombreDeJours
	 * @return liste de tableau de string a trois element
	 */
	public List<String[]> getFilmAndEmailSince(int nombreDeJours);

	/**
	 * Retourne un utilisateur a partir de son email
	 * @param email
	 * @return
	 */
	public Utilisateur getUtilisateur(String email);

	
	/////////////////////////////////////////
	//                                     //
	//  Gestion/Maintenance de l'automate  //
	//                                     //
	/////////////////////////////////////////
	
	
	/**
	 * Ajoute un acteur dans la BD
	 * @param nom
	 * @param prenom
	 * @param dateDeNaissance
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean ajoutActeur(String nom, String prenom, Date dateDeNaissance);

	/**
	 * Ajoute un auteur dans la BD
	 * @param nom
	 * @param prenom
	 * @param dateDeNaissance
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean ajoutAuteur(String nom, String prenom, Date dateDeNaissance);

	/**
	 * Ajoute un realisateur dans la BD
	 * @param nom
	 * @param prenom
	 * @param dateDeNaissance
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean ajoutRealisateur(String nom, String prenom, Date dateDeNaissance);

	/**
	 * Ajoute un DVD dans la BD
	 * @param dvd
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean ajoutDVD(DVD dvd);

	/**
	 * Ajoute un film dans la BD
	 * @param film
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean ajoutFilm(Film film);

	/**
	 * Mets  jour les films du mois
	 * @param filmsDuMois
	 * @return vrai si l'insertion s'est bien passe, faux sinon
	 */
	public boolean changementFilmMois(ArrayList<Film> filmsDuMois);

}
