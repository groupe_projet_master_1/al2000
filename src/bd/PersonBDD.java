package bd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import fc.Personnes.Personne;
import fc.films.Film;

public class PersonBDD {

	private static final PersonBDD INSTANCE = new PersonBDD();
	
	private PersonBDD() {}
	
	static PersonBDD getInstance() {
		return INSTANCE;
	} 

	long addPerson(Connection conn, String tableName, String nom, String prenom, Date dateDeNaissance) {
		long newId = -1;
		try { 
			newId = UtilsBDD.getInstance().getMaxId(conn, tableName);
			
			PreparedStatement ps = conn.prepareStatement("INSERT INTO " + tableName + " VALUES (?, ?, ?, ?)");
			
			ps.setLong(1, newId);
			ps.setString(2, nom);
			ps.setString(3, prenom);
			ps.setDate(4, dateDeNaissance);
			ps.executeQuery();
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
		return newId;
	}
	
	boolean addPersonRole(Connection conn, String tableName, long idFilm, long idPerson) {
		try { 
			PreparedStatement ps = conn.prepareStatement("INSERT INTO " + tableName + " VALUES (?, ?)");

			ps.setLong(1, idFilm);
			ps.setLong(2, idPerson);
			ps.executeQuery();
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Ajout d'une fonction pour obtenir un r�alisateur ou producteur ou acteur
	 * @param conn, tableNmae, idPerson
	 * @return Personne
	 * @throws SQLException
	 * */
	Personne getPersonne(Connection conn, String tableName, int idPerson) {
		Personne newPersonne = null;
		try { 
			PreparedStatement ps = conn.prepareStatement("SELECT nom, prenom, dateNaissance  FROM " + tableName + " where id=?");
			ps.setInt(1, idPerson);
			
			ResultSet r = ps.executeQuery();
		  	
			if(r.next()) {
				newPersonne  = UtilsBDD.getInstance().creerPersonne(r);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newPersonne;
	}
	
	/**
	 * Ajoute toutes les personnes qui ont joue, ecrit ou realise le film
	 * @param conn, f, idFIlm
	 * @return
	 * */
	void getMembresFilm(Connection conn, Film f, int idFilm) {
		for(Personne p: getProducteur(conn, idFilm)) {
			f.addProducteur(p);
		}
		for(Personne p: getRealisateur(conn, idFilm)) {
			f.addRealisateur(p);
		}
		for(Personne p: getActeur(conn, idFilm)) {
			f.addActeur(p);
		}
	}
	
	/**
	 * Renvoie tous les acteurs qui ont joue dans le film idFilm
	 * @param conn, idFilm
	 * @return ArrayList<Personne>
	 * @throws SQLException
	 * */
	ArrayList<Personne> getActeur(Connection conn, int idFilm){
		ArrayList<Personne> listePersonne = new ArrayList<>();
		try { 
			//V�rifie qu'il ne soit pas d�j� dans la table
			PreparedStatement ps = conn.prepareStatement("SELECT idActeur FROM AJoueDans where idFilm=?");
			ps.setInt(1, idFilm);
			
			ResultSet r = ps.executeQuery();
		  	
			while(r.next()) {
				listePersonne.add(getPersonne(conn, "LesActeurs", r.getInt("idActeur")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listePersonne;
	}
	
	/**
	 * Renvoie tous les realisateurs du film idFilm
	 * @param conn, idFilm
	 * @return ArrayList<Personne>
	 * @throws SQLException
	 * */
	ArrayList<Personne> getRealisateur(Connection conn, int idFilm){
		ArrayList<Personne> listePersonne = new ArrayList<>();
		try { 
			//V�rifie qu'il ne soit pas d�j� dans la table
			PreparedStatement ps = conn.prepareStatement("SELECT idRealisateur FROM ARealise where idFilm=?");
			ps.setInt(1, idFilm);
			
			ResultSet r = ps.executeQuery();
		  	
			while(r.next()) {
				listePersonne.add(getPersonne(conn, "LesRealisateurs", r.getInt("idRealisateur")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listePersonne;
	}
	
	/**
	 * Renvoie tous les producteurs du film idFilm
	 * @param conn, idFilm
	 * @return ArrayList<Personne>
	 * @throws SQLException
	 * */
	ArrayList<Personne> getProducteur(Connection conn, int idFilm){
		ArrayList<Personne> listePersonne = new ArrayList<>();
		try { 
			//V�rifie qu'il ne soit pas d�j� dans la table
			PreparedStatement ps = conn.prepareStatement("SELECT idAuteur FROM AEcrit where idFilm=?");
			ps.setInt(1, idFilm);
			
			ResultSet r = ps.executeQuery();
		  	
			while(r.next()) {
				listePersonne.add(getPersonne(conn, "LesAuteurs", r.getInt("idAuteur")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listePersonne;
	}

	long getIdPerson(Connection conn, String tableName, String nom, String prenom, Date date) {
		long res = -1;
		try { 
			PreparedStatement ps = conn.prepareStatement("SELECT id FROM " + tableName + " WHERE nom=? AND prenom=? AND dateNaissance=?");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setDate(1, date);
			
			ResultSet r = ps.executeQuery();
		  	
			if(r.next()) {
				res = r.getLong("id");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		return res;
	}
}
