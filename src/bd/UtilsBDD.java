package bd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import fc.Personnes.Personne;
import fc.carte.CarteAbonne;
import fc.compte.Abonnement;
import fc.compte.Utilisateur;
import fc.films.DVD;
import fc.films.Etats;
import fc.films.Film;

public class UtilsBDD {

	private static final UtilsBDD INSTANCE = new UtilsBDD();
	
	private UtilsBDD() {}
	
	static UtilsBDD getInstance() {
		return INSTANCE;
	} 
	
	/**
	 * Permet d'afficher une table
	 * @param rs
	 * @param largeurCellule
	 * @param nbCellule
	 * @throws SQLException 
	 */
	void afficherTab(ResultSet rs, int largeurCellule, int nbCellule) throws SQLException {
		while(rs.next()) {
			System.out.print("|");
			for(int i = 1; i <= nbCellule ; i++) {
				int j;
				int nbEspaceRestant = largeurCellule - rs.getString(i).length();
				for(j = 0 ; j < nbEspaceRestant/2 ;j++) {
					System.out.print(" ");
				}
				nbEspaceRestant -= j;
				System.out.print(rs.getString(i));
				for(j = 0 ; j < nbEspaceRestant ;j++) {
					System.out.print(" ");
				}
				System.out.print("|");
			}
			System.out.println();
			int largeurTotale = largeurCellule * nbCellule + nbCellule + 1;
			for(int i = 0; i < largeurTotale; i++) {
				System.out.print("-");
			}
			System.out.println();
		}
	}

	/**
	 * Permet d'obtenir l'id max d'une table
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	long getMaxId(Connection conn, String table) throws SQLException {
		Statement s = conn.createStatement();
		ResultSet rs = s.executeQuery("SELECT MAX(id) FROM " + table + "");
		if (rs.next()) {
			return rs.getLong("MAX(id)") + 1;
		} else {
			return 0;
		}
	}
	
	/**
	 * Permet d'obtenir le max d'une colonne d'une table
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	int getMax(Connection conn, String colonne, String table) throws SQLException {
		Statement s = conn.createStatement();
		ResultSet rs = s.executeQuery("SELECT MAX(" + colonne + ") FROM " + table + "");
		if (rs.next()) {
			return Integer.valueOf(rs.getString("MAX(" + colonne + ")")) + 1;
		} else {
			return 0;
		}
	}
	
	/**
	 * Permet de cr�er un film
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	Film creerFilm(Connection conn, ResultSet rs) throws SQLException {
		Film f = new Film(rs.getString("titre"));
		f.setDateParution(rs.getDate("dateDeParution"));
		f.setImage(rs.getString("imgUrl"));
		f.setId(rs.getInt("id"));
		PersonBDD.getInstance().getMembresFilm(conn, f, rs.getInt("id"));
		f.setSynopsis(rs.getString("synopsis"));
		f.setVideo(rs.getString("bandeAnnonceUrl"));
		return f;
	}
	
	/**
	 * Permet de cr�er un utilisateur
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	Utilisateur creerUtilisateur(ResultSet rs, ResultSet rsA) throws SQLException {
		Utilisateur u = new Utilisateur(rs.getString("email"));
		u.setNom(rs.getString("nom"));
		u.setPrenom(rs.getString("prenom"));
		u.setDateDeNaissance(rs.getDate("dateNaissance"));
		u.setMotDePasse(rs.getString("motDePasse"));
		u.setNumeroDeTelephone(rs.getString("numeroDeTelephone"));
		u.setEstEnfant(rs.getString("emailParent") == null);
		if (rsA.next()) {
			Abonnement a = new Abonnement(new Date(rsA.getDate("dateFinAbonnement").getTime()), new CarteAbonne(rsA.getString("numCarte")));
			a.setSolde(rsA.getFloat("solde"));
			u.setAbonnement(a);
		}
		return u;
	}

	/**
	 * Permet de cr�er une personne
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	Personne creerPersonne(ResultSet rs) throws SQLException {
		Personne p = new Personne(rs.getString("nom"), rs.getString("prenom"));
		p.setDateDeNaissance(rs.getDate("dateNaissance"));
		return p;
	}
	
	/**
	 * Permet de cr�er un dvd
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	DVD creerDVD(Connection conn, ResultSet rsDVD, ResultSet rsFilm) throws SQLException {
		DVD d = new DVD(rsDVD.getInt("idDVD"));
		d.setFilm(creerFilm(conn, rsFilm));
		d.setEtat(Etats.valueOf(rsDVD.getString("etat")));
		return d;
	}
	
}
