package bd;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import fc.compte.Abonnement;
import fc.compte.Utilisateur;
import fc.films.DVD;
import fc.films.Etats;
import fc.films.Film;
import fc.historique.Historique;

import org.apache.ibatis.jdbc.ScriptRunner;

public class GestionnaireBDD implements CommandeBDD {
	static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
	
	static final String USER = "";
	static public String PASSWD = "";
	
	static Connection conn; 
	public GestionnaireBDD() {
		// Enregistrement du driver Oracle
  	    System.out.print("Loading Oracle driver... "); 
  	    try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	    System.out.println("loaded");
  	    
  	    // Etablissement de la connection
  	    System.out.print("Connecting to the database... "); 
 	    try {
			conn = DriverManager.getConnection(CONN_URL,USER,PASSWD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   	    System.out.println("connected");
  	    
  	    // Desactivation de l'autocommit
	  	try {
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	    System.out.println("Autocommit disabled");
	}
	

	////////////////////////
	//  Table Management  //
	////////////////////////
	
	/**
	 * Supprime les tables si elles existent
	 */
	public void dropTable() {
		//Initialize the script runner
		ScriptRunner sr = new ScriptRunner(conn);
		//Creating a reader object
		Reader reader;
		try {
			reader = new BufferedReader(new FileReader("sql/dropTable.sql"));
			//Running the script
			sr.runScript(reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creation des tables si elles n'existent pas
	 */
	public void initTable() {
		//Initialize the script runner
		ScriptRunner sr = new ScriptRunner(conn);
		//Creating a reader object
		Reader reader;
		try {
			reader = new BufferedReader(new FileReader("sql/createTable.sql"));
			//Running the script
			sr.runScript(reader);
			reader = new BufferedReader(new FileReader("sql/insertTable.sql"));
			//Running the script
			sr.runScript(reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean ajoutAbonnementUtilisateur(Abonnement abonnement, Utilisateur user) {
		return Abo_CompteBDD.getInstance().addUserSubscription(conn, abonnement, user);
	}
	
	@Override
	public boolean emailDejaUtilise(String string) {
		return Abo_CompteBDD.getInstance().getUserByEmail(conn, string) != null;
	}
	
	@Override
	public boolean enregistrerUtilisateur(Utilisateur nouveau) {
		return Abo_CompteBDD.getInstance().addUser(conn, nouveau);
	}
	
	@Override
	public boolean louerFilm(DVD dvdaLouer) {
		return DVD_FilmBDD.getInstance().loanDVD(conn, dvdaLouer);
	}

	@Override
	public boolean modifierSolde(Utilisateur u, float montant) {
		return Abo_CompteBDD.getInstance().modifierSolde(conn, u, montant);
	}

	@Override
	public boolean retournerDvd(long dvdId, Etats e) {
		return DVD_FilmBDD.getInstance().returnDVD(conn, dvdId, e);
	}

	@Override
	public boolean retournerDvd(long dvdId, String email, Etats e, double note) {
		boolean b1 = DVD_FilmBDD.getInstance().returnDVD(conn, dvdId, e);
		boolean b2 = DVD_FilmBDD.getInstance().rateFilm(conn, DVD_FilmBDD.getInstance().getFilmIDFromDVDID(conn, dvdId), email, note);
		return b1 && b2;
	}

	@Override
	public boolean noterFilm(long idFilm, String email, double note) {
		return DVD_FilmBDD.getInstance().rateFilm(conn, idFilm, email, note);
	}
	
	@Override
	public DVD dispo(Film f) {
		return DVD_FilmBDD.getInstance().getDVDAvailable(conn, f);
	}
	
	@Override
	public List<Film> filtre(List<String> germe) {
		return DVD_FilmBDD.getInstance().getFilmByFilters(conn, germe);
	}
	
	@Override
	public List<Film> filmDisponibles() {
		return DVD_FilmBDD.getInstance().getMonthFilms(conn);
	}

	@Override
	public List<String[]> getFilmAndEmailSince(int nombreDeJours) {
		return DVD_FilmBDD.getInstance().getFilmAndEmailSince(conn, nombreDeJours);
	}
	
	@Override
	public Utilisateur getUtilisateur(String email) {
		return Abo_CompteBDD.getInstance().getUserByEmail(conn, email);
	}

	
	/////////////////////////////////////////
	//                                     //
	//  Gestion/Maintenance de l'automate  //
	//                                     //
	/////////////////////////////////////////

	
	@Override
	public boolean ajoutActeur(String nom, String prenom, Date dateDeNaissance) {
		return PersonBDD.getInstance().addPerson(conn, "LesActeurs", nom, prenom, dateDeNaissance) != -1;
	}

	@Override
	public boolean ajoutAuteur(String nom, String prenom, Date dateDeNaissance) {
		return PersonBDD.getInstance().addPerson(conn, "LesAuteurs", nom, prenom, dateDeNaissance) != -1;
	}
	
	@Override
	public boolean ajoutRealisateur(String nom, String prenom, Date dateDeNaissance) {
		return PersonBDD.getInstance().addPerson(conn, "LesRealisateurs", nom, prenom, dateDeNaissance) != -1;
	}
	
	@Override
	public boolean ajoutDVD(DVD dvd) {
		return DVD_FilmBDD.getInstance().addDVD(conn, dvd);
	}
	
	@Override
	public boolean ajoutFilm(Film film) {
		return DVD_FilmBDD.getInstance().addFilm(conn, film);
	}
	
	@Override
	public boolean changementFilmMois(ArrayList<Film> filmsDuMois) {
		boolean toReturn = true;
		toReturn = toReturn && DVD_FilmBDD.getInstance().clearMonthFilm(conn);
		for(Film film: filmsDuMois) {
			toReturn = toReturn && DVD_FilmBDD.getInstance().addMonthFilm(conn, film);
		}
		return toReturn;
	}

	
	public Historique getHistorique(Date date) {
		Historique h = null;
		return h;
	}
	
	public void ajoutHistorique(Historique h) {
		
	}

	
	
	/**
	 * Permet de modifier la date de fin d'abonnement d'un compte abonne
	 * @param u, nbMois
	 * @return boolean 
	 */
	public boolean majAbonnement(Utilisateur u, int nbMois) {
		return Abo_CompteBDD.getInstance().majAbonnement(conn, u, nbMois);
	}

	public static void main(String[] argv) {
		GestionnaireBDD gestionBDD = new GestionnaireBDD();

  	    //Supprime les tables existantes
		gestionBDD.dropTable();
  	    //Cree les tables
		gestionBDD.initTable();
	}

	
	
}
