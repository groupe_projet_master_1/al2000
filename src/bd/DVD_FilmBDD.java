package bd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fc.Personnes.Personne;
import fc.films.DVD;
import fc.films.Etats;
import fc.films.Film;

public class DVD_FilmBDD {
	
	private static final DVD_FilmBDD INSTANCE = new DVD_FilmBDD();
	
	private DVD_FilmBDD() {}
	
	static DVD_FilmBDD getInstance() {
		return INSTANCE;
	} 


	/**
	 * Ajoute un film dans la BD
	 * @param conn
	 * @param film
	 * @return vrai si l'insertion s'est bien passe
	 */
	boolean addFilm(Connection conn, Film film) {
		try {
			long newId = UtilsBDD.getInstance().getMaxId(conn, "LesFilms");
			
			PreparedStatement ps = conn.prepareStatement("INSERT INTO LesFilms VALUES (?, ?, ?, ?, ?)");
			
			ps.setLong(1, newId);
			ps.setString(2,film.getTitre());
			ps.setString(3, film.getVignetteURL());
			ps.setString(4, film.getBandeAnnonceURL());
			ps.setString(5, film.getSynopsis());
			ps.executeQuery();
			
			conn.commit();

			for (Personne p : film.getActeurs()) {
				long idPerson = PersonBDD.getInstance().addPerson(
						conn, 
						"LesActeurs", 
						p.getNom(), 
						p.getPrenom(), 
						new Date(p.getDateDeNaissance().getTime())
					);
				if (idPerson != -1) {
					PersonBDD.getInstance().addPersonRole(conn, "AJoueDans", newId, idPerson);
				} else {
					//Personne d�j� �xistant dans la table
					idPerson = PersonBDD.getInstance().getIdPerson(
							conn, 
							"LesActeurs", 
							p.getNom(), 
							p.getPrenom(), 
							new Date(p.getDateDeNaissance().getTime())
						);
					if (idPerson != -1) {
						PersonBDD.getInstance().addPersonRole(conn, "AJoueDans", newId, idPerson);
					} else {
						return false;
					}
				}
			}
			
			for (Personne p : film.getProducteur()) {
				long idPerson = PersonBDD.getInstance().addPerson(
						conn, 
						"LesAuteurs", 
						p.getNom(), 
						p.getPrenom(), 
						new Date(p.getDateDeNaissance().getTime())
					);
				if (idPerson != -1) {
					PersonBDD.getInstance().addPersonRole(conn, "AEcrit", newId, idPerson);
				} else {
					//Personne d�j� �xistant dans la table
					idPerson = PersonBDD.getInstance().getIdPerson(
							conn, 
							"LesAuteurs", 
							p.getNom(), 
							p.getPrenom(), 
							new Date(p.getDateDeNaissance().getTime())
						);
					if (idPerson != -1) {
						PersonBDD.getInstance().addPersonRole(conn, "AEcrit", newId, idPerson);
					} else {
						return false;
					}
				}
			}
			
			for (Personne p : film.getRealisateur()) {
				long idPerson = PersonBDD.getInstance().addPerson(
						conn, 
						"LesRealisateurs", 
						p.getNom(), 
						p.getPrenom(), 
						new Date(p.getDateDeNaissance().getTime())
					);
				if (idPerson != -1) {
					PersonBDD.getInstance().addPersonRole(conn, "ARealise", newId, idPerson);
				} else {
					//Personne d�j� �xistant dans la table
					idPerson = PersonBDD.getInstance().getIdPerson(
							conn, 
							"LesRealisateurs", 
							p.getNom(), 
							p.getPrenom(), 
							new Date(p.getDateDeNaissance().getTime())
						);
					if (idPerson != -1) {
						PersonBDD.getInstance().addPersonRole(conn, "ARealise", newId, idPerson);
					} else {
						return false;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Ajout d'un film du mois dans la BD
	 * @param conn
	 * @param film
	 * @return vrai si l'insertion s'est bien passe
	 */
	boolean addMonthFilm(Connection conn, Film film) {
		try { 
			PreparedStatement ps = conn.prepareStatement("INSERT INTO LesFilmsDuMois VALUES (?)");
			ps.setInt(1, (int)film.getId());
			ps.executeQuery();
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Ajoute un DVD dans la BD
	 * @param conn
	 * @param dvd
	 * @return vrai si l'insertion s'est bien passe
	 */
	boolean addDVD(Connection conn, DVD dvd) {
		try {
			PreparedStatement s = conn.prepareStatement("INSERT INTO LesDVD VALUES (?, ?, ?)");
			
			//id du DVD = sur la boite
			s.setLong(1, dvd.getId());
			s.setLong(2, dvd.getFilm().getId());
			s.setString(3, dvd.getEtat().toString());
			s.executeQuery();
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	long getFilmIDFromDVDID(Connection conn, long dvdID) {
		try {
			PreparedStatement s = conn.prepareStatement("SELECT idFilm FROM LesDVD WHERE idDVD=?");
			
			//id du DVD = sur la boite
			s.setInt(1, (int)dvdID);
			ResultSet rs = s.executeQuery();
			
			if (rs.next()) {
				return rs.getLong("idFilm");
			} else {
				return -1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	List<Film> getFilmByFilters(Connection conn, List<String> germe) {
		ArrayList<Film> toReturn = new ArrayList<>();
		
		//Construction de la requete de recherche
		String query = "SELECT * FROM LesFilms NATURAL JOIN LesFilmsDuMois";
		if(germe.size() != 0) {
			query = query.concat(" WHERE ");
			for (int i = 0; i < germe.size() - 1; i++) {
				query = query.concat("LOWER(titre) LIKE LOWER('%" + germe.get(i) + "%') OR ");
			}
			query = query.concat("LOWER(titre) LIKE LOWER('%" + germe.get(germe.size() - 1) + "%')");
		}
		
		try {
			Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery(query);
			while(rs.next()) {
				toReturn.add(UtilsBDD.getInstance().creerFilm(conn, rs));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toReturn;
	}

	/**
	 * Fonction non utilisee :(
	 * @param conn
	 * @param titre
	 * @return
	 */
	Film getFilmByTitle(Connection conn, String titre) {
		PreparedStatement s;
		try {
			s = conn.prepareStatement("SELECT * FROM LesFilms WHERE LOWER(titre)=LOWER(?)");
			s.setString(1, titre);
			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				return UtilsBDD.getInstance().creerFilm(conn, rs);
			} else {
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	List<Film> getMonthFilms(Connection conn) {
		ArrayList<Film> filmsDisponibles = new ArrayList<>();
		try {
			PreparedStatement s = conn.prepareStatement("select * from LesFilmsDuMois");
			
	  	    ResultSet r = s.executeQuery();
	  	    
		  	while (r.next()){
		  		PreparedStatement films = conn.prepareStatement("select * from LesFilms where id=?");
		  		films.setInt(1, r.getInt("id"));
		  		ResultSet r2 = films.executeQuery();
		  		
		  		while(r2.next()) {
		  			filmsDisponibles.add(UtilsBDD.getInstance().creerFilm(conn, r2));
		  		}
	  	    }
			
			//conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return filmsDisponibles;
	}
	
	/**
	 * Nettoie la table LesFilmsDuMois
	 * @param conn
	 * @return
	 */
	boolean clearMonthFilm(Connection conn) {
		try {
			PreparedStatement s = conn.prepareStatement("DELETE FROM LesFilmsDuMois WHERE id > 0");
			s.executeQuery();
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	boolean loanDVD(Connection conn, DVD dvd) {
		try {
			long newId = UtilsBDD.getInstance().getMaxId(conn, "LesTransactions");
			
			//On cree la transaction
			PreparedStatement ps = conn.prepareStatement("INSERT INTO LesTransactions (id, email, idDVD, dateDeb) VALUES (?, ?, ?, SYSDATE)");
			ps.setLong(1, newId);
			ps.setString(2, dvd.getEmail());
			ps.setLong(3, dvd.getId());
			ps.executeQuery();
			conn.commit();
			
			//Met a jour le dvd
			ps = conn.prepareStatement("update LesDVD set etat=? where idDVD=?");
			
			ps.setString(1, Etats.EMPRUNTE.toString());
			ps.setLong(2, dvd.getId());
			
			ps.executeQuery();
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	boolean returnDVD(Connection conn, long dvdId, Etats e) {
		try {
			PreparedStatement ps = conn.prepareStatement(
					"SELECT id FROM LesTransactions WHERE dateDeb="
					+ "(SELECT max(dateDeb) from LesTransactions WHERE idDVD=?)");
			ps.setLong(1, dvdId);
			ResultSet r = ps.executeQuery();
			
			if (r.next()) {
				ps = conn.prepareStatement("UPDATE LesTransactions SET dateFin=SYSDATE WHERE id=?");
				ps.setLong(1, r.getLong("id"));
				r = ps.executeQuery();
				conn.commit();
				
				return changeDVDState(conn, dvdId, e);
			} else {
				//Il y a eu une erreur lors de la requete
				return false;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Note le film d'id idFilm avec la note n
	 * @param conn
	 * @param idFilm
	 * @param n
	 * @return vrai si l'insertion s'est bien faite, faux sinon
	 */
	boolean rateFilm(Connection conn, long idFilm, String email, double n) {
		try {
			PreparedStatement ps = conn.prepareStatement("INSERT INTO LesNotes VALUES (?, ?, ?)");
			ps.setLong(1, idFilm);
			ps.setString(2, email);
			ps.setDouble(3, n);
			
			ps.executeQuery();
			
			conn.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Change l'etat du dvd d'id dvdId a l'etat e
	 * @param conn
	 * @param dvdId
	 * @param e
	 * @return vrai si la mise a jour s'est bien passe, faux sinon
	 */
	boolean changeDVDState(Connection conn, long dvdId, Etats e) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE LesDVD SET etat=? WHERE idDVD=?");
			
			ps.setString(1, e.toString());
			ps.setLong(2, dvdId);
			
			ps.executeQuery();
			conn.commit();
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Retourne le nom du film et l'email du locataire de chaque film non rendu il y a N jours
	 * @param conn
	 * @param nombreDeJours
	 * @return Liste de tableau a 3 �lements
	 */
	List<String[]> getFilmAndEmailSince(Connection conn, int nombreDeJours) {

		List<String[]> filmsAndEmails = new ArrayList<>();
		try {
			PreparedStatement ps = conn.prepareStatement(
					"SELECT titre, email, t.idDVD "
					+ "FROM LesFilms f, LesDVD d, LesTransactions t "
					+ "WHERE t.dateDeb = (SYSDATE-?) AND "
						+ "t.idDVD = d.idDVD AND "
						+ "d.idFilm = f.id AND "
						+ "t.dateFin IS NULL");
			
			ps.setInt(1, nombreDeJours);
			ResultSet r = ps.executeQuery();
			
			while (r.next()) {
				String t[] = {r.getString("t.idDVD"), r.getString("titre"), r.getString("email")};
				filmsAndEmails.add(t);
			}
			
			return filmsAndEmails;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return filmsAndEmails;
	}

	DVD getDVDAvailable(Connection conn, Film f) {
		DVD dvd = null;
		try {
			PreparedStatement DVD = conn.prepareStatement("SELECT * FROM LesDVD WHERE idFilm=? AND etat='DISPONIBLE'");

			DVD.setLong(1, f.getId());
			
	  	    ResultSet r = DVD.executeQuery();
	  	    
		  	if (r.next()){
				DVD newDVD = new DVD(f, r.getLong("idDVD"));
				newDVD.setEtat(Etats.valueOf(r.getString("etat")));
				dvd = newDVD;
	  	    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dvd;
	}
}
