package bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.sql.Date;

import fc.compte.Abonnement;
import fc.compte.Utilisateur;


public class Abo_CompteBDD {

	private static final Abo_CompteBDD INSTANCE = new Abo_CompteBDD();
	
	private Abo_CompteBDD() {}
	
	static Abo_CompteBDD getInstance() {
		return INSTANCE;
	} 

	/**
	 * Ajoute un utilisateur dans la base de donn�e
	 * @param conn
	 * @param newUser
	 */
	boolean addUser(Connection conn, Utilisateur newUser) {
		try {
			PreparedStatement ps = conn.prepareStatement("INSERT INTO LesComptes VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

			ps.setString(1, newUser.getEmail());
			if (!newUser.estEnfant()) {	
				ps.setString(2, null);//
			} else {
				ps.setString(2, newUser.getEmail());// newUser.getCompteParent());
			}
			ps.setString(3, newUser.getNom());
			ps.setString(4, newUser.getPrenom());
			ps.setString(5, newUser.getDateDeNaissance().toString());
			ps.setString(6, newUser.getMotDePasse());
			ps.setString(7, newUser.getAdresse());
			ps.setString(8, newUser.getNumeroDeTelephone());
			ps.executeQuery();
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Ajoute un nouvel abonnement dans la BD
	 * @param conn
	 * @param abonnement
	 * @param user
	 */
	boolean addUserSubscription(Connection conn, Abonnement abonnement, Utilisateur user) {
		try {
			int newId = UtilsBDD.getInstance().getMax(conn, "numCarte", "LesAbonnements");
			
			PreparedStatement ps = conn.prepareStatement("INSERT INTO LesAbonnements VALUES (?, ?, ?, ?)");

			ps.setString(1, user.getEmail());
			ps.setInt(2, 0);
			ps.setString(3, String.valueOf(newId));
			ps.setDate(4, new Date(abonnement.getDateFinAbonnement().getTime()));
			
			ps.executeQuery();
			
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Retourne un utilisateur � partir de son email
	 * @param conn
	 * @param email
	 * @return
	 */
	Utilisateur getUserByEmail(Connection conn, String email) {
		PreparedStatement s;
		try {
			s = conn.prepareStatement("SELECT * FROM LesComptes WHERE email=?");
			s.setString(1, email);
			ResultSet rs = s.executeQuery();
			s = conn.prepareStatement("SELECT * FROM LesAbonnements WHERE email=?");
			s.setString(1, email);
			ResultSet rsA = s.executeQuery();
			
			if (rs.next()) {
				return UtilsBDD.getInstance().creerUtilisateur(rs, rsA);
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Une erreur s'est produite
		return null;
	}
	
	/**
	 * Change le solde d'un compte, renvoi true si tout s'est bien passe
	 * @param conn
	 * @param u
	 * @param montant
	 * @return boolean
	 * */
	boolean modifierSolde(Connection conn, Utilisateur u, float montant) {
		try {
			String email = u.getEmail();
			String numCarte = u.getAbonnement().getCarteAbonne().getNumero();
			PreparedStatement s = conn.prepareStatement("SELECT solde FROM LesAbonnements WHERE email=? and numCarte=?");
			s.setString(1, email);
			s.setString(2, numCarte);
			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				float newSolde = rs.getFloat("solde") + montant;
				s = conn.prepareStatement("UPDATE LesAbonnements set solde=? where email=? and numCarte=?");
				s.setFloat(1, newSolde);
				s.setString(2, email);
				s.setString(3, numCarte);
				s.executeQuery();
				conn.commit();
				
				return true;
			} else {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Ajoute nbMois d'abonnement � l'utilisateur u
	 * @param conn
	 * @param u
	 * @param nbMois
	 * @return bolean 
	 * */
	boolean majAbonnement(Connection conn, Utilisateur u, int nbMois) {
		try {
			String email = u.getEmail();
			String numCarte = u.getAbonnement().getCarteAbonne().getNumero();
			PreparedStatement s = conn.prepareStatement("SELECT dateFinAbonnement FROM LesAbonnements WHERE email=? and numCarte=?");
			s.setString(1, email);
			s.setString(2, numCarte);
			
			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(rs.getDate("dateFinAbonnement"));
				calendar.add(Calendar.MONTH, nbMois);
				
				s = conn.prepareStatement("UPDATE LesAbonnements set dateFinAbonnement=? where email=? and numCarte=?");
				s.setDate(1, new Date(calendar.getTime().getTime()));
				s.setString(2, email);
				s.setString(3, numCarte);
				s.executeQuery();
			
				conn.commit();
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
