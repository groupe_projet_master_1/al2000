package fc;

public enum RetourLouer {
    LOUE,
    NON_CONNECTE,
    LIMITE_NB_LOCATIONS_DEPASSE,
    SOLDE_INSUFFISANT,
    FILM_NON_SELECTIONNE,
    DVD_INDISPO,
    PAIEMENT_ECHOUE
}