package fc.Personnes;

import java.util.Date;

public class Personne {
	String nom;
	String prenom;
	String adresse;
	Date dateDeNaissance;
	
	public Personne() {
		this("", "");
	}
	
	public Personne(String n, String p){
		nom = n;
		prenom = p;
	}
	
	public void setNom(String n) {
		nom = n;
	}
	
	public void setPrenom(String p) {
		prenom = p;
	}
	
	public void setAdresse(String a) {
		adresse = a;
	}
	
	public void setDateDeNaissance(Date ddn) {
		dateDeNaissance = ddn;
		
	}
	
	
	public String getNom() {
		return nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public String getAdresse() {
		return adresse;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	
	
	
}
