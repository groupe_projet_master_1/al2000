package fc.films;

public enum Genre {
	GUERRE,
	HORREUR,
	COMEDIE,
	ROMANTIQUE,
	AVENTURE,
	FANTASTIQUE,
}
