package fc.films;

/**
 * Liste d'�num�ration des �tats
 * @author Charly
 *
 */
public enum Etats {
	DISPONIBLE,
	EMPRUNTE,
	PERDU,
	ENDOMMAGE,
}
