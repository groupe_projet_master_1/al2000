package fc.films;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import fc.Personnes.Personne;

/**
 * Classe correspondant aux films
 * 
 * @author Charly
 *
 */
public class Film implements CommandeFilm {
	private long id;
	private String titre;
	private ArrayList<Personne> realisateur;
	private ArrayList<Personne> producteur;
	private ArrayList<Personne> acteur;
	private String synopsis;
	private double note;
	private int nbNotes;
	private Date dateParution;
	private ArrayList<Genre> genres;
	
	private String videoUrl;
	private String imageUrl;
	private BufferedImage image;

	private static final float NoteMax = 5;

	public Film(String titre) {
		this.titre = titre;

		realisateur = new ArrayList<>();
		producteur = new ArrayList<>();
		acteur = new ArrayList<>();

		note = 0;
		nbNotes = 0;

		genres = new ArrayList<>();
	}

	public void addRealisateur(Personne r) {
		realisateur.add(r);
	}

	public void setId(long i) {
		id = i;
	}

	public void addProducteur(Personne p) {
		producteur.add(p);
	}

	public void addActeur(Personne a) {
		this.acteur.add(a);
	}

	public void setVideo(String url) {
		videoUrl = url;
	}

	/**
	 * Change l'url de l'image et met � jour l'objet image en fonction
	 * @param url
	 */
	public void setImage(String url) {
		imageUrl = url;
		try {
			image = ImageIO.read(new URL(url));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setSynopsis(String s) {
		synopsis = s;
	}

	public long getId() {
		return id;
	}

	public String getVideo() {
		return videoUrl;
	}

	@Override
	public String getVignetteURL() {
		return imageUrl;
	}
	
	public BufferedImage getVignette() {
		return image;
	}

	public float getNoteMax() {
		return NoteMax;
	}

	public void addNote(int n) {
		note = (note * nbNotes + n) / (nbNotes + 1);
		nbNotes++;
	}

	public void removeNote(int n) {
		if (nbNotes - 1 > 0) {
			note = (note * nbNotes - n) / (nbNotes - 1);
			nbNotes--;
		} else {
			note = 0;
			nbNotes = 0;
		}
	}

	public void setDateParution(Date d) {
		dateParution = d;
	}

	public void addGenre(Genre g) {
		genres.add(g);
	}

	public String getTitre() {
		return titre;
	}

	@SuppressWarnings("unchecked")
	public List<Personne> getRealisateur() {
		return (List<Personne>) realisateur.clone();
	}

	@SuppressWarnings("unchecked")
	public List<Personne> getProducteur() {
		return (List<Personne>) producteur.clone();
	}

	@SuppressWarnings("unchecked")
	public List<Personne> getActeurs() {
		return (List<Personne>) acteur.clone();
	}

	public double getNote() {
		return note;
	}

	public Date getDateParution() {
		return dateParution;
	}

	@SuppressWarnings("unchecked")
	public List<Genre> getGenres() {
		return (List<Genre>) genres.clone();
	}

	@Override
	public String getBandeAnnonceURL() {
		return videoUrl;
	}

	@Override
	public int getNbNotes() {
		return nbNotes;
	}

	@Override
	public String getSynopsis() {
		return synopsis;
	}

}
