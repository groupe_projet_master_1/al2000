package fc.films;

import bd.UtilsBDD;

/**
 * Classe coresspondant au DVD
 * @author Charly
 *
 */
public class DVD {
	private long id;
	private Film film;
	private Etats etat;
	
	
	//dernier compte loueur du dvd
	private String email;
	
	public DVD(long id) {
		this.id = id;
		etat = Etats.DISPONIBLE;
	}
	
	public DVD(Film f, long id) {
		film = f;
		this.id = id;
		etat = Etats.DISPONIBLE;
	}
	
	//J'ai un doute sur l'utilite, on peut changer le film d'un DVD?
	public void setFilm(Film f) {
		film = f;
	}
	
	/**
	 * Change l'�tat du DVD
	 * @param e nouvel �tat
	 */
	public void setEtat(Etats e) {
		etat = e;
	}
	
	/**
	 * Change le dernier locataire
	 * @param e adresse mail du dernier locataire
	 */
	public void setEmail(String e) {
		email = e;
	}
	
	public long getId() {
		return id;
	}
	
	public Film getFilm() {
		return film;
	}
	
	public Etats getEtat() {
		return etat;
	}
	
	/**
	 * Retourne le dernier locataire
	 * @return
	 */
	public String getEmail() {
		return email;
	}
}
