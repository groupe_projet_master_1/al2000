package fc.films;

import java.util.Date;
import java.util.List;

import fc.Personnes.Personne;

public interface CommandeFilm {
	List<Personne> getActeurs();

	String getBandeAnnonceURL();

	String getVignetteURL();

	Date getDateParution();

	List<Genre> getGenres();

	int getNbNotes();

	double getNote();

	List<Personne> getProducteur();

	List<Personne> getRealisateur();

	String getSynopsis();

	String getTitre();

	float getNoteMax();

}
