package fc.historique;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fc.transaction.Transaction;

public class Historique{
	List<Transaction> listeTransactions;
	
	
	
	public Historique() {
		listeTransactions = new ArrayList<>();
	}
	
	
	
	public void ajoutTransaction(Transaction t) {
		if(!listeTransactions.add(t)) {
			System.out.println("probleme a l'ajout d'une transaction dans l'historique machine");
		};
	}

	public Iterator<Transaction> consulterHistorique() {
		return listeTransactions.iterator();
		
	}
	
	public void removeTransaction(int index) {
		listeTransactions.remove(index);
	}
}
