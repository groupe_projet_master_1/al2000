package fc.compte;

import java.util.Date;

import fc.carte.CarteAbonne;

/**
 * Classe d'abonnement
 * 
 * @author Mathis
 *
 */
public class Abonnement {
	private Date dateFinAbonnement;
	private CarteAbonne carte;
	private float solde;

	public Abonnement(Date dateFinAbonnement, CarteAbonne c) {
		this.carte = c;
		// initialisation de la date pour le calcul de l'abonnement
		this.dateFinAbonnement = dateFinAbonnement;
	}

	public void setDateFinAbonnement(Date d){
		dateFinAbonnement = d;
	}

	public Date getDateFinAbonnement() {
		return dateFinAbonnement;
	}

	public CarteAbonne getCarteAbonne() {
		return carte;
	}

	public boolean abonnementValide() {
		return this.dateFinAbonnement.compareTo(new Date()) <= 0;
	}

	public void setCarte(CarteAbonne carteAbonne) {
		this.carte = carteAbonne;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float s) {
		solde = s;
		
	}
}
