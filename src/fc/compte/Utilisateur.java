package fc.compte;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fc.Personnes.Personne;
import fc.carte.CarteAbonne;
import fc.historique.Historique;
import fc.transaction.Transaction;

public class Utilisateur extends Personne {

	private String email;
	private String motDePasse;
	private String numeroDeTelephone;
	private Historique h;
	private int nbFilmLoue;

	private static final int nbMaxFilmLoue = 5;
	public static final float prixAdherent = 4;
	public static final float prixNonAdherent = 5;

	public static final int EMAIL = 0;
	public static final int NOM = 4;
	public static final int PRENOM = 2;
	public static final int DATEDENAISSANCE = 3;
	public static final int MOTDEPASSE = 1;
	public static final int NUMERODETELEPHONE = 5;
	public static final int ADRESSE = 6;
	public static final int ESTENFANT = 7;

	private static final int NBANNEEMAJEUR = 18;
	private static final long NBSECONDEMAJEURS = (365 * 24 * 60 * 60) * NBANNEEMAJEUR;

	private Abonnement abonnement;

	private boolean estCompteEnfant;

	private ArrayList<Utilisateur> comptesEnfants;

	public Utilisateur(String email) {
		super();
		this.email = email;
		h = new Historique();
	}

	public void setEmail(String e) {
		email = e;
	}

	public void setMotDePasse(String mdp) {
		motDePasse = mdp;
	}

	public void setNumeroDeTelephone(String num) {
		numeroDeTelephone = num;
	}

	public void setEstEnfant(boolean estEnfant) {
		estCompteEnfant = estEnfant;
	}

	public void setNbFilmLoue(int n) {
		nbFilmLoue = n;
	}

	public void setSolde(float s) {
		abonnement.setSolde(s);
	}

	public void setAbonnement(Abonnement a) {
		this.abonnement = a;
	}

	// fin des seteurs
	/////////////////////////////////////////////////////////////////////////////////////////
	// debut geteurs

	public int getNbFilmLoue() {
		return nbFilmLoue;
	}

	public float getSolde() {
		return abonnement.getSolde();
	}

	public String getEmail() {
		return email;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public String getNumeroDeTelephone() {
		return numeroDeTelephone;
	}

	public Abonnement getAbonnement() {
		return abonnement;
	}

	// fin des geteurs
	/////////////////////////////////////////////////////////////////////////////////////////
	// debut des methodes utiles

	public void recharger(float n) {
		setSolde(getSolde() + n );
	}

	public void payer(float n) {
		setSolde(getSolde() - n );
	}

	public boolean estMineur() {
		return (Calendar.getInstance().getTime().getTime() - getDateDeNaissance().getTime()) >= NBSECONDEMAJEURS;
	}

	public boolean estEnfant() {
		return estCompteEnfant;
	}

	/**
	 * Permet d'acheter nbMois abonnements
	 * 
	 * @param nbMois - Nombre de mois d'abonnement
	 */
	public void acheterAbonnement(int nbMois) {
		// Calcule le mois de fin d'abonnement
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, nbMois);
		if (abonnement == null) {
			this.abonnement = new Abonnement(cal.getTime(), new CarteAbonne("0"));
		} else {
			abonnement.setDateFinAbonnement(cal.getTime());
		}

	}

	public void finAbonnement() {

	}

	public boolean estAbonne() {
		return abonnement.abonnementValide();
	}

	public void ajoutTransaction(Transaction t) {
		h.ajoutTransaction(t);
	}

	public boolean peutLouerDvd() {
		if (nbFilmLoue + 1 >= nbMaxFilmLoue)
			return false;
		else
			return true;
	}

	public boolean peutPayerDvd() {
		if ((estAbonne() && getSolde() < prixAdherent) || (!estAbonne() && getSolde() < prixNonAdherent))
			return false;
		else
			return true;
	}

	/**
	 * 
	 * @param avecSolde (true si paiement avec solde)
	 */
	public float loueDvd(boolean avecSolde) {
		nbFilmLoue++;
		if (avecSolde) {
			float prix;
			if (estAbonne()) {
				prix = prixAdherent;
			}
			else {
				prix = prixNonAdherent;
			}
			return prix;
		}
		else {
			return 0;
		}
	}
}
