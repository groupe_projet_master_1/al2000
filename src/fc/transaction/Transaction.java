package fc.transaction;
import java.util.Date;

public class Transaction {
	
	private long dvdId;
	private double tarif;
	private Date date;
	
	/**
	 * Une transaction regroupe les �l�ments essentiels � enregistrer 
	 * dans un historique.
	 */
	public Transaction () {
		dvdId = -1;
		tarif = -1;
		date = new Date(0);
	}
	
	/**
	 * @param id l'identifiant du DVD
	 */
	public void setDVDId(long id) {
		dvdId = id;
	}
	
	/**
	 * 
	 * @param t co�t de la location du DVD
	 */
	public void setTarif(double t) {
		tarif = t;
	}
	
	/**
	 * 
	 * @param d date de la transaction
	 */
	public void setDate(Date d) {
		date = d;
	}
	
	/**
	 * Retourne l'identifiant du DVD concern� par la transaction.
	 * 
	 * @return l'identifiant du DVD
	 */
	public long getDVDId() {
		return dvdId;
	}
	
	/**
	 * Retourne le co�t de la location du DVD concern� par la transaction.
	 * 
	 * @return le co�t de la location
	 */
	public double getTarif() {
		return tarif;
	}
	
	/**
	 * Retourne la date � laquelle la transaction � eu lieu.
	 * 
	 * @return la date de la transaction
	 */
	public Date getDate() {
		return date;
	}
	
}
