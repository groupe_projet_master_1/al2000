package fc;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import bd.CommandeBDD;
import bd.GestionnaireBDD;
import fc.compte.Utilisateur;
import fc.films.CommandeFilm;
import fc.films.DVD;
import fc.films.Etats;
import fc.films.Film;
import fc.historique.Historique;
import fc.transaction.Transaction;
import fc.carte.CarteBleu;

public class Machine implements CommandeMachine {
	private Historique h;
	private CommandeBDD bd;

	private int filmCourant;
	private List<Film> selection;

	private Utilisateur utilisateur;
	private String emailInput;
	private DVD dvdaLouer;
	private CarteBleu cb;

	private EtatsAuthentification etatAuthentification;
	private ModePaiement modePaiement;
	private RetourLouer retourLouer;

	private static final float prixAdherent = 4;
	private static final float prixNonAdherent = 5;
	private static final float prixAboParMois = 1;

	private List<TransactionListener> ListeTransaction = new ArrayList<>();

	public Machine() {
		init();
	}

	/////////////////////////////////////////////////////////////////////////
	// methode privee de la classe

	private void init() {
		h = new Historique();
		bd = new GestionnaireBDD();
		tri("");
		etatAuthentification = EtatsAuthentification.NON_CONNECTE;
	}

	private void setEmailInput(String email) {
		emailInput = email;
	}

	private void setUtilisateur(Utilisateur u) {
		utilisateur = u;
	}

	private Film filmCourant() {
		return selection.get(filmCourant);
	}

	private DVD dvdDispo(Film f) {
		return bd.dispo(f);
	}

	private boolean paiement() {
		if (modePaiement == ModePaiement.CB) {
			return cb.paiement(utilisateur.estAbonne());
		} else {
			if (utilisateur.estAbonne()) {
				utilisateur.loueDvd(true);
				bd.modifierSolde(utilisateur, -prixNonAdherent);
				return true;
			} else {
				utilisateur.loueDvd(false);
				bd.modifierSolde(utilisateur, -prixAdherent);
				return true;
			}
		}
	}

	private boolean verifieEmail(String email) {
		setUtilisateur(bd.getUtilisateur(email));
		if (utilisateur == null) {
			etatAuthentification = EtatsAuthentification.LOGIN_INCONNU;
			return false;
		} else {
			etatAuthentification = EtatsAuthentification.LOGIN_OK;
			return true;
		}
	}

	private boolean addTransactionLocation(DVD d) {
		Transaction n = new Transaction();
		n.setDVDId(d.getId());
		n.setDate(Calendar.getInstance().getTime());
		n.setTarif(utilisateur.estAbonne() ? prixNonAdherent : prixAdherent);
		utilisateur.ajoutTransaction(n);
		h.ajoutTransaction(n);
		return true;
	}

	private boolean emailDejaUtilise(String email) {
		return bd.emailDejaUtilise(email);
	}

	private List<String> traitementRecherche(String s) {
		String[] rt = s.toLowerCase().split(" ", 0);
		List<String> r = new ArrayList<>();
		for (int i = 0; i < rt.length; i++) {
			if (rt[i].length() != 0) {
				r.add(rt[i]);
			}
		}
		return r;
	}

	////////////////////////////////////////////////////////////////////////////////
	// methode input/output de la classe

	@Override
	public ModePaiement getModePaiement() {
		return modePaiement;
	}

	@Override
	public void setModePaiement(ModePaiement m) {
		modePaiement = m;
	}

	@Override
	public boolean setDonneesCB(String donnee[]) {
		if (modePaiement == ModePaiement.CB) {
			cb = new CarteBleu(donnee[CarteBleu.NUMERO], donnee[CarteBleu.EXPI], donnee[CarteBleu.CRYPTO]);
			return true;
		} else
			return false;
	}

	@Override
	public void setFilmCourant(int index) {
		filmCourant = index;
	}

	public String getEmailInput() {
		return emailInput;
	}

	public ModePaiement getModePaiementt() {
		return modePaiement;
	}

	public float getPrixAdherent() {
		return prixAdherent;
	}

	public float getPrixNonAdherent() {
		return prixNonAdherent;
	}

	public float getAboParMois() {
		return prixAboParMois;
	}

	@Override
	public CommandeFilm getFilmCourant() {
		return filmCourant();
	}

	@Override
	public List<String> getToutesVignettesURL() {
		List<String> l = new ArrayList<>();
		for (Film f : selection) {
			l.add(f.getVignetteURL());
		}
		return l;
	}
	
	@Override
	public List<BufferedImage> getToutesVignettes() {
		List<BufferedImage> l = new ArrayList<>();
		for (Film f : selection) {
			l.add(f.getVignette());
		}
		return l;
	}

	@Override
	public RetourLouer getRetourLouer() {
		return retourLouer;
	}

	@Override
	public EtatsAuthentification getEtatsAuthentification() {
		return etatAuthentification;
	}

	@Override
	public boolean estAdmin() {
		return etatAuthentification == EtatsAuthentification.ADMIN;
	}

	public boolean estAbonne() {
		return utilisateur.estAbonne();
	}


	/////////////////////////////////////////////////////////////////////////////////
	// methode utile de la classe

	@Override
	public boolean dispoFilm() {
		return dvdDispo(filmCourant()) != null;
	}

	@Override
	public void tri(String s) {
		List<String> germe = traitementRecherche(s);
		selection = bd.filtre(germe);
	}

	public EtatsAuthentification authentification(String email, String mdp) {
		setEmailInput(email);
		if (mdp.equals("admin") && email.equals("admin")) {
			etatAuthentification = EtatsAuthentification.ADMIN;
		}
		else if (verifieEmail(email)) {
			if (mdp.equals(utilisateur.getMotDePasse())) {
				etatAuthentification = EtatsAuthentification.CONNECTE;
			} else {
				etatAuthentification = EtatsAuthentification.MDP_FAUX;
			}
		}
		return etatAuthentification;
	}

	public RetourLouer louerDVD() {
		if(filmCourant() == null){
			retourLouer = RetourLouer.FILM_NON_SELECTIONNE;
			return retourLouer;
		}
		if (etatAuthentification != EtatsAuthentification.CONNECTE) {
			retourLouer = RetourLouer.NON_CONNECTE;
			return retourLouer;
		}
		dvdaLouer = dvdDispo(filmCourant());
		if(dvdaLouer == null){
			retourLouer = RetourLouer.DVD_INDISPO;
			return retourLouer;
		}

		if (modePaiement == ModePaiement.SOLDE && (utilisateur.peutPayerDvd() == false)) {
			retourLouer = RetourLouer.SOLDE_INSUFFISANT;
			return retourLouer;
		}

		if (utilisateur.peutLouerDvd() == false) {
			retourLouer = RetourLouer.LIMITE_NB_LOCATIONS_DEPASSE;
			return retourLouer;
		}

		if (paiement()) {
			dvdaLouer.setEmail(utilisateur.getEmail());
			bd.louerFilm(dvdaLouer);
			addTransactionLocation(dvdaLouer);
			retourLouer = RetourLouer.LOUE;
			for (TransactionListener transactionListener : ListeTransaction) {
				transactionListener.TransactionEffectue();
			}
			return retourLouer;
		}
		retourLouer = RetourLouer.PAIEMENT_ECHOUE;
		return retourLouer;
	}

	@Override
	public void deconnexion() {
		utilisateur = null;
		etatAuthentification = EtatsAuthentification.NON_CONNECTE;
		emailInput = null;
		dvdaLouer = null;
		modePaiement = null;
		retourLouer = null;
		cb = null;
	}

	@Override
	public void addListenerTransaction(TransactionListener t) {
		ListeTransaction.add(t);
	}

	@Override
	public boolean retournerDvd(int idDvd) {
		bd.retournerDvd(idDvd, Etats.DISPONIBLE);
		return true;
	}

	@Override
	public boolean retournerDvd(int idDvd, double note) {
		bd.retournerDvd(idDvd, utilisateur.getEmail(), Etats.DISPONIBLE, note);
		return true;
	}

	@Override
	public boolean inscription(String donnee[]) {
		if (emailDejaUtilise(donnee[Utilisateur.EMAIL])) {
			return false;
		} else {
			Utilisateur nouveau = new Utilisateur(donnee[Utilisateur.EMAIL]);
			nouveau.setNom(donnee[Utilisateur.NOM]);
			nouveau.setPrenom(donnee[Utilisateur.PRENOM]);
			try {
				nouveau.setDateDeNaissance(
						new SimpleDateFormat("dd/MM/yyyy").parse(donnee[Utilisateur.DATEDENAISSANCE]));
			} catch (ParseException e) {
				// traitement d'erreur a ajouter
			}
			nouveau.setMotDePasse(donnee[Utilisateur.MOTDEPASSE]);
			nouveau.setNumeroDeTelephone(donnee[Utilisateur.NUMERODETELEPHONE]);
			nouveau.setAdresse(donnee[Utilisateur.ADRESSE]);
			nouveau.setEstEnfant(Boolean.parseBoolean(donnee[Utilisateur.ESTENFANT]));
			setUtilisateur(nouveau);
			bd.enregistrerUtilisateur(nouveau);
			return true;
		}
	}

	@Override
	public boolean abonnement(int nbMois) {
		if (etatAuthentification != EtatsAuthentification.CONNECTE)
			return false;
		if (modePaiement != ModePaiement.CB)
			return false;
		cb.payer(nbMois * prixAboParMois);
		utilisateur.acheterAbonnement(nbMois);
		bd.ajoutAbonnementUtilisateur(utilisateur.getAbonnement(), utilisateur);
		return true;
	}


	@Override
	public boolean rechargerSolde(float montant) {
		if(montant <= 0) {
			return false;
		}
		if (etatAuthentification != EtatsAuthentification.CONNECTE)
			return false;
		if (modePaiement != ModePaiement.CB)
			return false;
		if(cb == null || !cb.payer(montant)) {
			return false;
		}
		return bd.modifierSolde(utilisateur, montant);
	}

	// gestion/maintenance automate
	public boolean ajoutFilm(Film film) {
		if (estAdmin()) {
			bd.ajoutFilm(film);
			return true;
		}
		return false;
	}

	public boolean ajoutDVD(DVD dvd) {
		if (estAdmin()) {
			bd.ajoutDVD(dvd);
			return true;
		}
		return false;
	}

	public boolean changementFilmMois(ArrayList<Film> filmsDuMois) {
		if (estAdmin()) {
			bd.changementFilmMois(filmsDuMois);
			return true;
		}
		return false;
	}

	public boolean ajoutActeur(String nom, String prenom, java.sql.Date dateDeNaissance) {
		if (estAdmin()) {
			bd.ajoutActeur(nom, prenom, (java.sql.Date) dateDeNaissance);
			return true;
		}
		return false;
	}

	public boolean ajoutAuteur(String nom, String prenom, java.sql.Date dateDeNaissance) {
		if (estAdmin()) {
			bd.ajoutAuteur(nom, prenom, (java.sql.Date) dateDeNaissance);
			return true;
		}
		return false;
	}

	public boolean ajoutRealisateur(String nom, String prenom, java.sql.Date dateDeNaissance) {
		if (estAdmin()) {
			bd.ajoutRealisateur(nom, prenom, (java.sql.Date) dateDeNaissance);
			return true;
		}
		return false;
	}

	public void majEtatDvdPerdu(){
		List<String[]> liste = bd.getFilmAndEmailSince(14);
		for (String[] strings : liste) {
			bd.retournerDvd(Long.valueOf(strings[0]), Etats.PERDU);
		}
	}

}
