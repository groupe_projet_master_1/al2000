package fc.carte;

public class CarteAbonne extends Carte {
	private String numero;

	public CarteAbonne(String numero) {
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String n){
		numero = n;
	}

}
