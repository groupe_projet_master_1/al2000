package fc.carte;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import fc.compte.Utilisateur;

public class CarteBleu {
	private String numero;
	private Date expiration;
	private int cryptogramme;

	public static final int NUMERO = 0;
	public static final int EXPI = 1;
	public static final int CRYPTO = 2;

	public CarteBleu(String n, String e, String c) {
		numero = n;
		try {
			expiration = new SimpleDateFormat().parse(e);
		} catch (ParseException parseExcept) {

		}
		cryptogramme = Integer.parseInt(c);
	}

	public boolean paiement(boolean estAbonne) {
		if (estAbonne) {
			return payer(Utilisateur.prixAdherent);

		} else {
			return payer(Utilisateur.prixNonAdherent);
		}

	}

	public boolean payer(float montant) {
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("vous venez de payer " + montant + "€ avec votre carte bleu" );
		return true;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public Date getExpiration() {
		return expiration;
	}
	
	public int getCryptogramme() {
		return cryptogramme;
	}

}
