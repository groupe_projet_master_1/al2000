package fc;

import java.awt.image.BufferedImage;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import fc.films.CommandeFilm;
import fc.films.DVD;
import fc.films.Film;

public interface CommandeMachine {
	
	////////////////////////////////////////////////////////////
	// input/output
	
	/**
	 * 
	 * @param donnee String[3] avec assignation grace aux constante de la classe CarteBleu
	 * @return
	 */
	boolean setDonneesCB(String donnee[]);

	/**
	 * enregistre comme film courant le film ayant pour indice index dans la selection de film faite par tri
	 * @param index indice dans la liste de film 
	 */
	void setFilmCourant(int index);
	
	/**
	 * 
	 * @return LOGIN_INCONNU, LOGIN_OK, MDP_FAUX, CONNECTE, NON_CONNECTE
	 */
	EtatsAuthentification getEtatsAuthentification();

	/**
	 * renvoi le film selectionne prealablement par setFilmCourant, sinon resultat incoherent
	 * @return
	 */
	CommandeFilm getFilmCourant();

	/**
	 * retourne l'ensemble des URL des vignettes des films selectionne prealablement par tri
	 * Si aucun tri alors renvoi la vignette de tout les films
	 * @return liste de vignettes des films selectionnes
	 */
	List<String> getToutesVignettesURL();
	
	/**
	 * retourne l'ensemble des vignettes des films selectionne prealablement par tri
	 * Si aucun tri alors renvoi toutes les vignettes
	 * @return liste de vignettes des films selectionnes
	 */
	List<BufferedImage> getToutesVignettes();
	
	/**
	 * 
	 * @return LOUE, NON_CONNECTE, LIMITE_NB_LOCATIONS_DEPASSE, SOLDE_INSUFFISANT,
	 *         DVD_INDISPO
	 */
	RetourLouer getRetourLouer();
	
	/**
	 * renvoi l'email precedemment rentre par une tentative de connexion
	 * @return email precedemment rentre
	 */
	String getEmailInput();

	/**
	 * renvoi le mode de paiement choisi precedement par l'utilisateur
	 * @return
	 */
	ModePaiement getModePaiementt();
	
	/** informe si l'utilisateur actif est abonne
	 * @return
	 */
	boolean estAbonne();

	/** informe quel mode de paiement est enregistre
	 * @return
	 */
	ModePaiement getModePaiement();
	
	/**
	 * renvoi le prix d'un adherent pour la location d'un dvd
	 * @return	prix
	 */
	float getPrixAdherent();

	/**
	 * renvoi le prix d'un non adherent pour la location d'un dvd
	 * @return	prix
	 */
	float getPrixNonAdherent();

	/**
	 * retourne le prix d'un abonnement par mois
	 * @return prix
	 */
	float getAboParMois();

	

	/**
	 * informe si l'utilisateur actif est un admin
	 * @return
	 */
	boolean estAdmin();

	
	
	////////////////////////////////////////////////////////////////////////////////////////::
	// methode utile

	/**
	 * fait un tri dans les films disponible du mois de la machine en fonction de la chaine s
	 * @param s chaine de characteres definissant le tri
	 * change le retour de getToutesVignettesURL() ainsi que getFilmCourant() (getFilmCourant ne correspond plus a un film choisi, a ne pas utiliser 
	 * si pas de rappel a setFilmCourant
	 */
	void tri(String s);


	/**
	 * 
	 * @param donnee String[8] avec assignation grace aux constantes de la classe utilisateur
	 * @return
	 */
	boolean inscription(String[] donnee);

	/**
	 * Permet à l'utilisateur de s'authentifier, verifie si son email est dans la bd
	 * et si le mdp est correcte
	 * 
	 * @param email
	 * @param mdp
	 * @return renvoi l'etat de l'authentification
	 */
	EtatsAuthentification authentification(String email, String mdp);

	/**
	 * Permet de louer le dvd precedement selectionne par setFilmCourant(int index)
	 * 
	 * @return LOUE si la location a reussi, modifie la valeur de retourLouer
	 */
	RetourLouer louerDVD();

	/**
	 * Deconnecte l'utilisateur
	 */
	void deconnexion();

	/**
	 * informe si un dvd du film selectionne est disponible (si ce film est louable)
	 * @return
	 */
	boolean dispoFilm();


	/** Permet de retourner un dvd grace a son code barre, il n'est pas necessaire
	 * d'etre connecte
	 * 
	 * @param idDvd
	 * @return true si l'action a reussi
	 */
	boolean retournerDvd(int idDvd);

	/**
	 * Retour dvd et notation du film precondition note : nombre de 0 a 5 (etoile,
	 * demi-etoile acceptee )
	 * 
	 * @param idDvd
	 * @param note
	 * @return
	 */
	boolean retournerDvd(int idDvd, double note);

	/**
	 * S'abonner ou se reabonner nbMois
	 * precondition : mode de paiement CB et utilisateur connecte
	 * @param nbMois
	 * @return
	 */
	boolean abonnement(int nbMois);
	
	/**
	 * recharge le solde de l'utilisateur actif
	 * precondition : avoir mis le mode de paiement en CB et avoir renseigne les donnee de cb
	 * @param montant a prelever et a ajouter au solde courant
	 * @return	informe si tout s'est bien passe
	 */
	boolean rechargerSolde(float montant);
	
	/**
	 * 
	 * @param t
	 */
	void addListenerTransaction(TransactionListener t);

	/**
	 * modifie le mode de paiement
	 * @param m
	 */
	void setModePaiement(ModePaiement m);


	//gestion de la base de données des films
	boolean ajoutFilm(Film film);

	boolean ajoutDVD(DVD dvd);

	boolean changementFilmMois(ArrayList<Film> filmsDuMois);

	boolean ajoutActeur(String nom, String prenom, Date dateDeNaissance);

	boolean ajoutAuteur(String nom, String prenom, Date dateDeNaissance);

	boolean ajoutRealisateur(String nom, String prenom, Date dateDeNaissance);

}
