package fc;

public enum EtatsAuthentification {
    LOGIN_INCONNU,
    LOGIN_OK,
	MDP_FAUX,
	CONNECTE,
	NON_CONNECTE,
	ADMIN
}
